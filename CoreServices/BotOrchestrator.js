'use strict';

// Imports and initalizations
var AIFactory = require('../AI/AIFactory.js').AIFactory;
var c_aifactory = new AIFactory();

var express = require('express');
var app = express();
var bodyParser = require('body-parser');

var ProcessorFactory = require('../Processors/ProcessorFactory.js').ProcessorFactory;
var c_processorfactory = new ProcessorFactory();

const LanguageIdentifier = require('./LanguageIdentifier.js').LanguageIdentifier;
var c_languageidentifier = new LanguageIdentifier();

var ConfigurationManager = require('../PlatformAPI/ConfigurationManager/ConfigurationManager.js').ConfigurationManager;
var c_configurationmanager =  new ConfigurationManager();

//Load Enterprise Entity Logger
var EnterpriseEntityLogger = require('../PlatformAPI/EnterpriseEntityLogger/EnterpriseEntityLogger.js').EnterpriseEntityLogger;
var c_enterpriseentitylogger = new EnterpriseEntityLogger();

//Load Entity Status Manager
var EntityStatusManager = require('../PlatformAPI/EntityStatusManager/EntityStatusManager.js').EntityStatusManager;
var c_entitystatusmanager = new EntityStatusManager();

//Load Entity states enum
var ComponentStatus = require('../PlatformAPI/Utilities/ComponentStatus.js').ComponentStatus;

var c_aiinstance={};
var c_aiinstance2={};
var c_processorinstance={};
let c_entityname = 'BotOrchestrator';
let c_instancename = '';
var c_logger=require('../PlatformAPI/Utilities/Logger.js')(module);

// Instance of Mongo DB adapter
var MongoDBManager = require('../PlatformAPI/Utilities/MongoDBManager.js').MongoDBManager;

class BotOrchestrator
{
	
	constructor(){}
	
	// Initiatialise user interaction
initialiseCoreServices(p_instancename, coreServicesCallback)
{
	try{
			c_logger.info("Entering initialiseCoreServices method");
			//c_aiinstance = c_aifactory.getAiHandler(p_instancename);
			c_instancename = p_instancename;
			c_entitystatusmanager.updateEntityState(p_instancename,c_entityname,ComponentStatus.STARTING,function(){					
				c_enterpriseentitylogger.insertEntityLog(p_instancename,c_entityname,'Starting Bot Orchestrator','ENTITY_STATE',function(success){
					c_logger.info("Entity log Inserted for (instance:"+p_instancename+", entity:"+c_entityname+"):"+success);
				});
				c_logger.info('Starting Bot Orchestrator');
				
				let l_paramnames = ['orchestrator-host','orchestrator-port'];
				c_configurationmanager.getConfigParamsByListUsingNames(p_instancename,c_entityname,l_paramnames,function(p_configparammap){
					if(p_configparammap != null || p_configparammap !='' || p_configparammap != undefined )
					{
							let l_orchestratorhost = p_configparammap.get('orchestrator-host');
							let l_orchestratorport = p_configparammap.get('orchestrator-port');
							
							app.use(bodyParser.json());
							
							try{
									var server = app.listen(l_orchestratorport,l_orchestratorhost, function () {

										var host = server.address().address
										var port = server.address().port

										c_logger.info('BotOrchestrator app listening at http://'+host+':'+port);
									
									
										c_aifactory.getAiHandler(p_instancename, function(p_aiinstance) {
											if(p_aiinstance != null || p_aiinstance != '')
											{
												
												c_aiinstance = p_aiinstance;
											}
											else{
												c_logger.error("Error occurred :: Class - BotOrchestrator :: Method - getAiHandler() :: p_aiinstance not found ");
											}
																		
										});							
																
										c_processorfactory.getProcessorInstance(p_instancename, function(p_processorinstance){
											if(p_processorinstance != null || p_processorinstance != '')
											{
												c_logger.info('Got processor instance:'+p_processorinstance);
												c_processorinstance = p_processorinstance;
											}
											else{
												c_logger.error("Error occurred :: Class - BotOrchestrator :: Method - getProcessorInstance() :: p_processorinstance not found ");
											}
										});	
										c_logger.info("Exiting initialiseCoreServices method");
										coreServicesCallback("Success");
									});
							}
							catch(e)
							{
								c_logger.info("Class - BotOrchestrator :: method - initialiseCoreServices() ",e);
							}
							
						c_entitystatusmanager.updateEntityState(p_instancename,c_entityname,ComponentStatus.STARTED,function(){						
							c_enterpriseentitylogger.insertEntityLog(p_instancename,c_entityname,'Bot Orchestrator has started','ENTITY_STATE',function(success){
								c_logger.info("Entity log Inserted for (instance:"+p_instancename+", entity:"+c_entityname+"):"+success);
							});
							c_logger.info('Started Bot Orchestrator');
							
							app.post('/BotOrchestrator/processMessage',function(request,response){
								
								c_logger.info('Inside /BotOrchestrator/processMessage service');
								
								//response.setHeader('Content-Type', 'application/json');
								let p_commonobject = request.body;
								
								c_enterpriseentitylogger.insertEntityLog(c_instancename,c_entityname,'Received message from channel in orchestrator: '+ p_commonobject.text,'DEBUG',function(success){
									c_logger.debug("Entity log Inserted:"+success);
								});
								c_logger.debug('Received object from channel: '+ p_commonobject.text);
								
								c_logger.debug('sessionid-------------------'+p_commonobject.sessionid+'--------------------');
								//processUserInteraction(p_commonobject, responseCallback){
								//c_logger.info("Entering processUserInteraction method");

								//To be Changed
								//c_languageidentifier.findTongue(p_commonobject.text,c_instancename, function(p_language){
									//if(p_language != null){
											//c_logger.info("Entering callback function:findTongue method ");
														
											c_aiinstance.handleAIInteraction(p_commonobject, function(p_airesponse){
													if(p_airesponse != null || p_airesponse!= '')
													{
														c_logger.info("Entering callback function:handleAIInteraction method");
														//Ajinkya
														c_processorinstance.manageProcessing(p_commonobject, p_airesponse, p_airesponse.language, c_aiinstance, function(p_responseText){
															if(p_responseText != null || p_responseText != ''){
																	c_logger.info("Entering callback function:manageProcessing method");
																	
																	//Setting response text in common object
																	p_commonobject.responsetext = p_responseText;
																	
																	//Send the response object
																	//response.type('application/json');							
																	response.end(JSON.stringify(p_commonobject));
																	
																	c_enterpriseentitylogger.insertEntityLog(p_instancename,c_entityname,'Response sent to channel from orchestrator: '+ p_responseText,'DEBUG',function(success){
																		c_logger.debug("Entity log Inserted:"+success);
																	});
																	c_logger.debug('Response sent to channel from orchestrator: '+ p_responseText);
																	
																	//Formulate the object to persist in messages collection
																	var l_messageobject = {};
																	l_messageobject['text'] = p_commonobject.text;					
																	l_messageobject["botResponse"]=p_commonobject.responsetext;
																	l_messageobject["botMessageTimeStamp"] = new Date();
																	l_messageobject["botName"]=c_instancename;
																	
																	 c_configurationmanager.getConfigParamByName(c_instancename, 'BotOrchestrator','messages-collection',function(p_messagecollection){
																		if(p_messagecollection!=null || p_messagecollection !=''){
																				c_logger.info("Entering callback function:getConfigParamByName method");
																				
																				//Config Params
																				let l_paramnamesformongo = ['mongo-db-host','mongo-db-port','mongo-db-name'];		
														
																				//API call to retreive all the config param values in Map
																				c_configurationmanager.getConfigParamsByListUsingNames(p_instancename, 'CommonConfig', l_paramnamesformongo,function(p_configparammap){
																					if(p_configparammap != null ||p_configparammap != ''){
																						var l_url = 'mongodb://'+p_configparammap.get('mongo-db-host')+':'+p_configparammap.get('mongo-db-port')+'/'+p_configparammap.get('mongo-db-name') ;
																						var mongoDBManager = new MongoDBManager(l_url);
																						
																						mongoDBManager.insertMessageInCollection(l_messageobject, p_messagecollection ,function(p_booleanresponse){
																							if(p_booleanresponse != null || p_booleanresponse!= ''){
																								c_logger.info("Entering callback function:insertMessageInCollection method");
																								//responseCallback(p_commonobject);
																								c_logger.info("Exiting callback function:insertMessageInCollection method");
																							}
																							else{
																								c_logger.error("Error occurred :: Class - BotOrchestrator :: Method - insertMessageInCollection() :: p_booleanresponse not found ");
																							}
																							
																						});
																						c_logger.info("Exiting callback function:getConfigParamsByListUsingNames method");
																					}
																					else{
																						c_logger.error("Error occurred :: Class - BotOrchestrator :: Method - getConfigParamsByListUsingNames() :: p_configparammap not found ");
																					}
																					
																				});																								
																				c_logger.info("Exiting callback function:getConfigParamByName method");
																				
																		}
																		else{
																			c_logger.error("Error occurred :: Class - BotOrchestrator :: Method - getConfigParamByName() :: p_messagecollection not found ");
																		}
																	
																	});
																	c_logger.info("Exiting callback function:manageProcessing method");
															}
															else{
																c_logger.error("Error occurred :: Class - BotOrchestrator :: Method - manageProcessing() :: p_responseText not found ");
															}
													
														});
														c_logger.info("Exiting callback function:handleAIInteraction method");
													}
													else{
														c_logger.error("Error occurred :: Class - BotOrchestrator :: Method - handleAIInteraction() :: p_airesponse not found ");
													}
											
												}); 
												c_logger.info("Exiting callback function:findTongue method ");
									/* }
									else{
										c_logger.error("Error occurred :: Class - BotOrchestrator :: Method - findTongue() :: p_language not found ");
									} */
								
							//	});
								c_logger.info("Exiting processUserInteraction method");	
							});
							
							app.get('/hello',function(request,response){
								response.send('hello');
							});
							
						});
					}
					else{
						c_logger.error("Error occurred :: Class - BotOrchestrator :: Method - initialiseCoreServices() :: p_configparammap not found ");
					}
				});
			
			});
		}				
		catch(e)
		{
			c_logger.error("Error occurred :: Class - Channel Factory :: Method - getEntityNameByType() ",e);
		}					
			
	}
}


module.exports.BotOrchestrator = BotOrchestrator;

process.argv.forEach(function (val, index, array) {	
		
	if(array.length == 2)
	{
		c_logger.info("Please provide instance name to initialize!!!!");
		process.exit(1);
		return;			
	}
	
	// Take the bot name as a parameter
	if(index === 2)
	{				
		let l_instancename = val;
		c_logger.info("Initializing instance:" + l_instancename);
		let l_botorchestrator = new BotOrchestrator();
		l_botorchestrator.initialiseCoreServices(l_instancename,function(status){
			c_logger.info('BotOrchestrator startup status:'+status);
			c_enterpriseentitylogger.insertEntityLog(l_instancename,c_entityname,'BotOrchestrator initialised','INFO',function(success){
				c_logger.info("Entity log Inserted for (instance:"+l_instancename+", entity:"+c_entityname+"):"+success);
			});
		});
	}
		
});