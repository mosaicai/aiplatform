"use strict";

// Properties reader
var ConfigurationManager = require('../PlatformAPI/ConfigurationManager/ConfigurationManager.js').ConfigurationManager;
var c_configurationmanager = new ConfigurationManager();

var c_logger = require('../PlatformAPI/Utilities/Logger.js')(module);
const request = require('request');
let msRequest =  request.defaults({
	headers :{
				"Ocp-Apim-Subscription-Key":'4dae135aeaa246558f712e18cbca6bae'
		   	}
});




let language='en';

let checkIfKnownLanguage= function(languageObj){
	return languageObj.value === language;
}

class LanguageIdentifier{
	
	constructor(){}
	
	// Find the language of a given utterance
	findTongue(message,p_instancename, callback){
	
	
		if(message === ''){
			callback('en');
		}
		c_logger.info("Entering findTongue method");
		try{
				c_configurationmanager.getConfigParamByName(p_instancename, 'ApiAi', 'language-token', function(p_languagetoken){
					
				 if(p_languagetoken!= null || p_languagetoken!= ''){
				 
						let jsonBody = {
						"documents": [
							{
							"id": message.user,
							"text": message
							}
						]
					};
					let options = {
						//'url':loadedConfiguration.findProperty('linguist.endPointUrl'),
						'url':p_languagetoken,
						'json':jsonBody
					};
					msRequest.post(options , function(error, response, body){
						
						if(error){
							c_logger.info("Exiting findTongue method");
							callback('en');
						}
						else{
							
							if(body.documents instanceof Array){
								
								if(body.documents.length>0){
									language= body.documents[0].detectedLanguages[0].iso6391Name;
									c_logger.info('found the language as : ' + language);
									c_logger.info("Exiting findTongue method");				
									callback(language);
								}
								else{
									c_logger.info("Exiting findTongue method");
									callback('en');
								}
							}
							else{
								c_logger.info("Exiting findTongue method");
								callback('en');
							}
						}
					});
				
				 }
				 else{
					c_logger.error("Error occured :: Class - LanguageIdentifier :: Method - getConfigParamByName() :: p_languagetoken not found");
				 }
			
				});
		}
		catch(e)
		{
			c_logger.error("Error occured :: Class - LanguageIdentifier :: Method - findTongue() ",e);
		}
	}
}

module.exports.LanguageIdentifier=LanguageIdentifier;