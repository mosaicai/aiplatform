var winston = require('winston');
winston.emitErrs = true;
//var config = require('config');
//var path = config.get('AppSettings.logger_path');
var c_logfilepath='./Categorization_Logs.log';

var util = require('./Util.js');
var bootstrapDir = '';
var path = require('path');
var c_callingmodule;

var getLabel = function() {
    var parts = c_callingmodule.filename.split(path.sep);    
    return parts[parts.length - 2] + '/' + parts.pop();
};

var logger = function(callingmodule){
	c_callingmodule = callingmodule;
	return new winston.Logger({
    transports: [
        new winston.transports.File({
			timestamp:function(){
				return new Date(Date.now()).toString('yyyy-MM-ddTHH:mm:ss');
			},
			label: getLabel(),
            level: 'debug',
            filename: c_logfilepath,
            handleExceptions: true,
            json: false,
            maxsize: 5242880, //5MB
            maxFiles: 5,
            colorize: false
        }),
        new winston.transports.Console({
            level: 'debug',
            handleExceptions: true,
            json: false,
            colorize: true
        })
    ],
    exitOnError: false
});
};

module.exports = logger;
module.exports.stream = {
    write: function(message, encoding){
        logger.info(message);
    }
};


