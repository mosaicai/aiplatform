var mongodb = require('mongodb');
//var config = require('config');
var c_logger = require('./logger.js')(module);
var MongoClient = mongodb.MongoClient;
//var url = config.get('AppSettings.mongo_url');

	
module.exports = {		
	
	//fetch bigrams from mongo	
	retrieveBigrams : function(p_url,callback){
		MongoClient.connect(p_url, function (err, db) {
		c_logger.info('retrieveBigrams begin');
			var arr = [];
			if (err)
				c_logger.error('Unable to connect to the mongoDB server. Error:', err);
			else 
			{
				var stream = db.collection('GroupedBiGrams').find().stream();
				stream.on('data', function(item) {
					arr.push(item);
				});
								
				stream.on('end', function() {
					db.close();
					c_logger.debug('Bigram Data :: '+arr);
					callback(arr);
				});
			}
		});
	},

	//fetch training data from mongo	
	retrieveTrainingdata : function(p_url,callback){
		MongoClient.connect(p_url, function (err, db) {
			var arr = [];
			if (err)
				c_logger.error('Unable to connect to the mongoDB server. Error:', err);
			else 
			{
				var stream = db.collection('TrainingData').find().stream();
				stream.on('data', function(item) {
					arr.push(item);
				});
				
				stream.on('end', function() {
					db.close();
					callback(arr);
				});
			}
		});
	}			
		
};