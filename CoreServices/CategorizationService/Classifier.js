var Cosine = require('cosine');
var MongoAdapter = require('./DataStudio.js');
var c_logger = require('./Logger.js')(module);
var HashMap = require('hashmap');
var Natural = require('natural');
var Pos = require('pos');
nounInflector = new Natural.NounInflector();
nounInflector.attach();

//bigram words loaded from mongo into an object 
function bigram(callback){
	var l_bigram = MongoAdapter.bigramData;
	c_logger.info('bigram data fetched from mongo')
	callback(l_bigram);
}

//training data loaded from mongo into an object 
function training_data(callback){
	var l_trainingData = MongoAdapter.trainingData;
	c_logger.info('training data fetched from mongo');
		callback(l_trainingData);
}

module.exports = {
	
	//single utterance processing
	classifySingleUtterance : function(p_token,p_utterance,p_incident_id, callback){
		processing(p_utterance,p_incident_id,function(p_data){			
			callback(p_data);
		});	
	}
	
};

//finding the category for given utterance
function processing(p_utterance,p_id,callback){
try{
	var l_matchedWords = [];
	var l_uniGramArray = [];
	var l_arr = [];
	var l_cosineValuesArray = [];
	var l_pluralToSingularMap = new HashMap();
	var l_cosineValuesMap = new HashMap();
	var l_originalUtterance = p_utterance;
	
	
	bigram(function(p_bigram_map){
		p_bigram_map.forEach(function(p_value,p_key){
			var l_words = p_value.toString().replace(/,/igm,'|');
			var l_regex = new RegExp(l_words, 'igm');
			l_matchedWords.push(p_utterance.match(l_regex));
		});

		if(l_matchedWords !== null && l_matchedWords !== []){
			for(var i=0;i<l_matchedWords.length;i++){
				if(l_matchedWords[i] !== null){	
					var l_word = l_matchedWords[i];
					l_word = l_word.toString().replace(/ /g,'');
					l_uniGramArray[i] = l_word;
				}
			}
				
			//replaces the bi/tri gram word with uni gram word
			for(var i=0;i<l_matchedWords.length;i++){
				for(var j=0;j<l_uniGramArray.length;j++){
					p_utterance = p_utterance.replace(l_matchedWords[i],l_uniGramArray[i]);
				}
			}
		}
					
		var l_words = new Pos.Lexer().lex(p_utterance.toUpperCase());
		var l_tags = new Pos.Tagger()
		.tag(l_words)
		.map(function(tag){
			l_pluralToSingularMap.set(tag[0],tag[1]);
		})
					
		//replace the plural words from the utterance to singular words
		l_pluralToSingularMap.forEach(function(p_value, p_key) {
			if(p_value === 'NNS' || p_value === 'NNPS'){
				p_utterance = p_utterance.toUpperCase().replace(p_key,p_key.singularizeNoun());
			}	
		});
				
		//retrieving the training data from hashmap
		training_data(function(p_trainingData_map){
			p_trainingData_map.forEach(function(p_value,p_key){
				l_cosineValuesMap.set(p_key,Cosine(p_utterance.toLowerCase().split(/\s|\_|\,|\.|\@|\-|\:|\;|\/|\'|\(|\)/),p_value.toLowerCase().split(/\s/)));
			});

			c_logger.info('finding relevant categories for the given user utterance:'+utterance)	
			l_cosineValuesMap.forEach(function(p_value,p_key){
				if(p_value > 0){
					l_cosineValuesArray.push(p_value);
				}	
			});

			l_cosineValuesArray = l_cosineValuesArray.sort();
				
			//finding the top three cosine scores(values)
			var l_p = l_cosineValuesArray[l_cosineValuesArray.length - 1]
			l_arr.push(l_p);//highest cosine value
			l_arr.push(l_cosineValuesArray[l_cosineValuesArray.length - 2]);//second highest cosine value
			l_arr.push(l_cosineValuesArray[l_cosineValuesArray.length - 3]);//third highest cosine value
	
			var l_categories='';
			var l_category='';
			
			l_cosineValuesMap.forEach(function(p_value,p_key){
				if(p_value > 0){
					for(var i=0;i<l_arr.length;i++){
						if(l_arr[i] === p_value){
							if(p_key !== 'IT Infrastructure:Service Requests'){
								l_categories = l_categories+','+p_key;
							}	
						}	
						if(p_value === l_p){
							l_category = p_key;
						}
					}	
				}
			});	

			l_categories = l_categories.substr(1,l_categories.length);
			if(l_category === 'IT Infrastructure:Service Requests'){
				callback(p_utterance.toLowerCase()+'->'+'I can only guide you related to incident requests'+'***'+''+'%%%'+p_id);//If category is IT Infrastructure:Service Requests
			}else{
				callback(p_utterance.toLowerCase()+'->'+l_category+'***'+l_categories+'%%%'+p_id);//categories other than Service Requests
			}	
		});
	});
	}
	catch(e)
	{
		c_logger.info('Error occured :: Class - Classifier :: Method :: processing()')
	}
	
}
