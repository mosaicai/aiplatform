var Express = require('express');
var c_app = Express();
var BodyParser = require('body-parser');
var c_logger = require('./Logger.js')(module);
var Classifier = require('./Classifier.js');

c_app.use(BodyParser.json());

	//service to accept single utterance in JSON format
	c_app.post('/getCategory',  function (req, res) {
			try{
				//Prepare output in JSON format
				if(req!=null && req!='')
				{
						token = req.body.token;
						utterance = req.body.utterance;
						incident_id = req.body.incidentId;
						Classifier.classifySingleUtterance(token,utterance,incident_id, function(data){
							var split = data.split('***');
							var split1 = split[0].split('->');//split1[0] contains the user utterance & split1[1] contains the category with highest score
							var split2 = split[1].split('%%%');//split2[0] contains the relevant categories
							// send resposne
							if(split2[0] !== ''){
								//res.send('User Utterance:'+split1[0]+'\n'+'Category:'+split1[1]+'\n'+'Categories:'+split2[0]+'\n'+'Incident Id:'+split2[1]);
								c_logger.debug('category:'+split2[0]);
								res.send(split2[0]);
							}else{
								//res.send('User Utterance:'+split1[0]+'\n'+'Response:'+split1[1]+'\n'+'Incident Id:'+split2[1]);
								c_logger.debug('category:'+split1[1]);
								res.send(split1[1]);
							}	
						});
				}
				else{
					c_logger.error("Class - Server :: Method - app.post(/getcategory) :: req object no found");
				}
			}
			catch(e)
			{
					c_logger.error("Class - Server :: Method - app.post(/getcategory)");
			}
	});



//code to start the server
module.exports = {		
		startServer : function(p_portnumber,callback){
		var server = c_app.listen(p_portnumber, function () {
		   var host = server.address().address;
		   var port = server.address().port;
		   callback("Application is listening at http:"+host+":"+port);
		})
	}	
}
