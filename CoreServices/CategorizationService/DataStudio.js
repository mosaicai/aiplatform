'use strict';
var MongoAdapter = require('./MongoAdapter.js');
var c_server = require('./Server.js');
var HashMap = require('hashmap');
var c_logger = require('./Logger.js')(module);
var c_bigrammap = new HashMap();
var c_trainingdatamap = new HashMap();

//Loading Configuration Manager
var ConfigurationManager = require('../../PlatformAPI/ConfigurationManager/ConfigurationManager.js').ConfigurationManager;
var c_configurationmanager =  new ConfigurationManager();

//Loading Enterprise Entity Logger
var EnterpriseEntityLogger = require('../../PlatformAPI/EnterpriseEntityLogger/EnterpriseEntityLogger.js').EnterpriseEntityLogger;
var c_enterpriseentitylogger = new EnterpriseEntityLogger();

//Load Entity Status Manager
var EntityStatusManager = require('../../PlatformAPI/EntityStatusManager/EntityStatusManager.js').EntityStatusManager;
var c_entitystatusmanager = new EntityStatusManager();

//Load Entity states enum
var ComponentStatus = require('../../PlatformAPI/Utilities/ComponentStatus.js').ComponentStatus;

let c_entityname = 'CategorizationService';
let c_paramnames = ['server-port','mongo-db-name','mongo-db-port','mongo-db-host'];
let c_url = '';

process.argv.forEach(function (val, index, array) {	
		
	if(array.length == 2)
	{
		c_logger.info("Please provide instance name to initialize!!!!");
		process.exit(1);
		return;			
	}
	
	// Take the bot name as a parameter
	if(index === 2)
	{				
		let l_instancename = val;
		c_configurationmanager.getConfigParamsByListUsingNames('ithelpdeskfbbot',c_entityname,c_paramnames,function(p_configparammap){
			if(p_configparammap!=null && p_configparammap!='')
			{
						try{
					
						c_url = 'mongodb://'+p_configparammap.get('mongo-db-host')+":"+p_configparammap.get('mongo-db-port')+"/"+p_configparammap.get('mongo-db-name');	
						c_logger.debug("mongodb url for categorization:"+c_url);
						//loads the bigrams from object into a hashmap	
						MongoAdapter.retrieveBigrams(c_url,function(bigram){
							//loads the training data from object into a hashmap
						c_logger.debug("Biagram :: "+bigram);
						if(bigram!=null && bigram!='')
						{
							MongoAdapter.retrieveTrainingdata(c_url,function(p_trainingData){
							
								if(p_trainingData!=null && p_trainingData!='')
								{
										c_logger.debug("Inside retrieveTrainingdata:"+p_trainingData);
										for(var i=0;i < bigram.length;i++){
											c_bigrammap.set(bigram[i].Category,bigram[i].Bigram);
										}

										for(var i=0;i < p_trainingData.length;i++){
											c_trainingdatamap.set(p_trainingData[i].Category,p_trainingData[i].Keywords);
										}			
													
										c_entitystatusmanager.updateEntityState('ithelpdeskfbbot',c_entityname,ComponentStatus.STARTING,function(p_statusmessage){
											c_logger.info('Starting Categorization service.');
											c_enterpriseentitylogger.insertEntityLog('ithelpdeskfbbot',c_entityname,'Starting Categorization Service','INFO',function(success){					
											});
											
											//once the data is loaded into hashmaps the server gets started
											c_server.startServer(p_configparammap.get('server-port'),function(status){
												c_logger.info('Class- DataStudio :: '+status);
												c_entitystatusmanager.updateEntityState('ithelpdeskfbbot',c_entityname,ComponentStatus.STARTED,function(p_statusmessage){
													c_logger.info('Started Categorization service.');
													c_enterpriseentitylogger.insertEntityLog('ithelpdeskfbbot',c_entityname,'Started Categorization Service','INFO',function(success){					
													});
												});
											});							
											
										});			
								
								}
								else{
									c_logger.debug("Class- DataStudio :: p_trainingData not found");
								
								}
								
							});	
						}
						else{
							c_logger.error("Error occured :: Class - DataStudio :: bigram not found");
						}
						});
					}
					catch(e)
					{
							c_logger.error('Error occured :: Class - Datastudio :: method -getConfigParamsByListUsingNames',e);
					}
					
			}
			else{
				c_logger.error('Error occured :: Class - Datastudio :: method -getConfigParamsByListUsingNames :: no params retrieved from config map');
			}
		});
	}
});

//the loaded hashmaps are exported
module.exports.bigramData = c_bigrammap;
module.exports.trainingData = c_trainingdatamap;