# Share properties
[appServer]
# uiSocketIP=localhost
uiSocketIP=localhost
uiSocketPort=200


channels=1
sampleRate=44100
bitDepth=16
persistenceMode=file
filePath=audio/
audioFilePrefix=audio
audioFiletype=wav

jarvisSocketTimeOut=60000
jarvisError=Hi, there was an issue connecting with the chat server, please try again in some time
