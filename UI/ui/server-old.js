'use strict';
var PropertiesReader = require('properties-reader');

var express = require('express');
var app = express();

var https = require('https');

var io;

var fs = require('fs');
var CheckLoginDetails = require('./checkLoginDetails.js').CheckLoginDetails;
var checkLoginDetails = new CheckLoginDetails();
var sharedProperties = PropertiesReader("../config/shared-properties.js");

var mongoProperties = PropertiesReader("./public/config/mongo-collections.js");
var loginCollection = mongoProperties.get('login-detail-collection');
var searchCollection = mongoProperties.get('search-detail-collection');
var botname = mongoProperties.get('bot-name');

var MongoConnection = require('./public/js/mongodb-connection.js').MongoConnection;
var mongoConnection = new MongoConnection();

var objectId='';
var userName='';
var email='';
var firstName='';
var secondName='';
var newStatus='';
var gender='';
var botAccessArray;
var searchResult;
var botSearchArray;
var botIdArray;

var url='';
var hostName = '';
class UIServer
{
	constructor(mode)
	{   
	
		var session = require('express-session');
		 
		app.use(session({
			secret: '2C44-4D44-WppQ38S',
			resave: true,
			saveUninitialized: true
		}));

		var mongoLoginData = function(data) {
			console.log("login details captured. "+data);
		}

		// Authentication and Authorization Middleware
		var auth = function(req, res, next) {

		  if (req.session && req.session.validUser)
			return next();
		  else
			return res.sendStatus(401);
		};
	    
		app.use(express.static('public'));
		var cors = require('cors');
        app.use(cors());

		app.get('/', function(req, res){
			res.sendFile('login.html', { root: "public" } );
		});
		
		app.get('/loadData', function(req, res){
			res.send(req.session);
		});
		
		//search functionality
		app.get('/search', function(req, res){
			
			var botSearchArray = function(data)	{				
				searchResult=data.searchStatus;
				botSearchArray=data.botArray;
				res.setHeader('Content-Type', 'application/json');
				res.send(JSON.stringify({ 'searchResult': searchResult, 'botSearchArray': botSearchArray  }));			
			}

			var search =  req.query.search;
			console.log("Search String-------------->"+search);
			mongoConnection.search(search, searchCollection, botSearchArray);
			
		});
		
		//SAP bot
		app.get('/selfservice/kedb',  function(req, res){
		if(req.session.objectId != null && req.session.objectId !='' && req.session.objectId != undefined && req.session.objectId != 'undefined') {				
				console.log('In if-----' + req.session.objectId);
				req.session.valid = true;				
				res.sendFile('jarvis-voice.html', { root: "public/selfservice/SAP" } );
			} else {				
				console.log('In else-----' + req.session.objectId);
				req.session.valid = false;				
			}
		});
		
		//VIACOM bot
		app.get('/selfservice/faq',  function(req, res){
			if(req.session.objectId != null && req.session.objectId !='' && req.session.objectId != undefined && req.session.objectId != 'undefined') {
			req.session.valid = true;
			res.sendFile('jarvis-voice.html', { root: "public/selfservice/VIACOM" } );
			} else {
				console.log('In else-----' + req.session.objectId);
				req.session.valid = false;
				res.redirect(url);
			}
		});
		
		//JarvisPA
		app.get('/selfservice/helpdesk',  function(req, res){
			if(req.session.objectId != null && req.session.objectId !='' && req.session.objectId != undefined && req.session.objectId != 'undefined') {
			req.session.valid = true;
			res.sendFile('jarvis-voice.html', { root: "public/selfservice/JarvisPA" } );
			} else {
				console.log('In else-----' + req.session.objectId);
				req.session.valid = false;
				res.redirect(url);
			}
		});

		//salesforce bot
		app.get('/selfservice/contactmanagement',  function(req, res){
			if(req.session.objectId != null && req.session.objectId !='' && req.session.objectId != undefined && req.session.objectId != 'undefined') {
			req.session.valid = true;
			res.sendFile('jarvis-voice.html', { root: "public/selfservice/salesforce" } );
			} else {
				console.log('In else-----' + req.session.objectId);
				req.session.valid = false;
				res.redirect(url);
			}
		});
		
		//png_sap bot
		app.get('/selfservice/png_sap',  function(req, res){
			if(req.session.objectId != null && req.session.objectId !='' && req.session.objectId != undefined && req.session.objectId != 'undefined') {
			req.session.valid = true;
			res.sendFile('jarvis-voice.html', { root: "public/selfservice/png_sap" } );
			} else {
				console.log('In else-----' + req.session.objectId);
				req.session.valid = false;
				res.redirect(url);				
			}
		});
		//absa-bot
		app.get('/selfservice/hello_money_bot',  function(req, res){
			if(req.session.objectId != null && req.session.objectId !='' && req.session.objectId != undefined && req.session.objectId != 'undefined') {
			req.session.valid = true;
			res.sendFile('jarvis-voice.html', { root: "public/selfservice/hello_money_bot" } );
			} else {
				console.log('In else-----' + req.session.objectId);
				req.session.valid = false;
				res.redirect(url);				
			}
		});
		
		//mercer-bot
		app.get('/selfservice/mercer',  function(req, res){
		if(req.session.objectId != null && req.session.objectId !='' && req.session.objectId != undefined && req.session.objectId != 'undefined') {				
				console.log('In if-----' + req.session.objectId);
				req.session.valid = true;				
				res.sendFile('jarvis-voice.html', { root: "public/selfservice/Mercer_Bot" } );
			} else {				
				console.log('In else-----' + req.session.objectId);
				req.session.valid = false;				
			}
		});
		
		//sappngdemobot bot
		app.get('/selfservice/sappngdemobot',  function(req, res){
		if(req.session.objectId != null && req.session.objectId !='' && req.session.objectId != undefined && req.session.objectId != 'undefined') {				
				console.log('In if-----' + req.session.objectId);
				req.session.valid = true;				
				res.sendFile('jarvis-voice.html', { root: "public/selfservice/sappngdemobot" } );
			} else {				
				console.log('In else-----' + req.session.objectId);
				req.session.valid = false;				
			}
		});

                 //beverage bot
		app.get('/selfservice/beverage',  function(req, res){
		if(req.session.objectId != null && req.session.objectId !='' && req.session.objectId != undefined && req.session.objectId != 'undefined') {				
				console.log('In if-----' + req.session.objectId);
				req.session.valid = true;				
				res.sendFile('jarvis-voice.html', { root: "public/selfservice/beverage" } );
			} else {				
				console.log('In else-----' + req.session.objectId);
				req.session.valid = false;				
			}
		});



		//botlist page after successful login
		app.get('/botlist', auth, function(req, res){
			res.sendFile('bot-list.html', { root: "public" } );
		});
		
		//logout functionality
		app.get('/logout', function(req, res){
			req.session.valid = false;
			req.session.objectId = null;
			req.session.destroy();
            var logoutData={"userId": objectId ,"timestamp":new Date(),"action":"logout"};
			mongoConnection.insertMessageInCollection(logoutData , loginCollection, mongoLoginData );
			res.sendFile('login.html', { root: "public" } );
		});
			
		//login action to validate user
		app.get('/login', function (req, res) {
  
			var status = 'failure';
  			if(null !=req ){
			hostName = req.get('host');
			console.log("hostname========"+hostName);
			var email =  req.query.email;
			var password =  req.query.password;	
			console.log("email  >> "+email+" password >>"+ password);
			var returnStatus = function(status, userId, user_name, firstName, secondName, botAccess, gender_value,bot_id) {
				botIdArray=bot_id;
				console.log("bot ids are"+botIdArray);
				gender=gender_value;
				objectId = userId;
				email = user_name;
				newStatus=status;
				botAccessArray = botAccess; 
				userName=firstName.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
				if(status==="Success") {
					console.log("Inside success>>>"+status);
					var loginData={"userId":userId,"timestamp":new Date(),"action":"login"}
					req.session.validUser = true;
					req.session.objectId = objectId;
					req.session.userName = userName;
					req.session.gender = gender_value;
					mongoConnection.insertMessageInCollection(loginData,loginCollection, mongoLoginData );
					res.redirect('botlist');
					
				} else if (status==="Failure"){
					console.log("Inside failure>>>"+status);
					req.session.validUser = false;
					req.session.destroy();
					var failedLoginData={"userId":userId,"timestamp":new Date(),"action":"invalidLogin"};
					mongoConnection.insertMessageInCollection(failedLoginData ,loginCollection, mongoLoginData );
					res.sendFile('login.html', { root: "public" });
				}
			}
			var checkLoginDetails = new CheckLoginDetails();
			if(null != email && null != password && email != '' && password != ''){
					console.log("username >> "+email+"password >> "+password);
					var status = checkLoginDetails.validateUser(email, password, returnStatus);
				}
	
  
			}
  
 
		});
		
		if ('secure' == mode)
		{
			let webAppsPort=sharedProperties.get('appServer.uiSocketPort');
			let httpsServer = https.createServer({
			key: fs.readFileSync('../certs/key.pem'),
			cert: fs.readFileSync('../certs/cert.pem')
			},app).listen(webAppsPort);

                     //let httpsServer = https.createServer({
			//cert: fs.readFileSync('../certs/star_lntinfotech_com.cer'),
                     //ca: [ fs.readFileSync('../certs/GeoTrust_True_BusinessID_Wildcard_Intermediate _Certificate.cer'), 'password']
			//},app).listen(webAppsPort);
			
			io = require('socket.io')(httpsServer);
			io.on('connection', function(socket){
				console.log('a user connected');
			
				socket.on('login_msg', function(msg){
					console.log('message: ' + msg);
					socket.emit('userId',objectId);
					socket.emit('username',userName);
					socket.emit('status',newStatus);
					socket.emit('botAccess',botAccessArray);
					socket.emit('gender',gender);
					socket.emit('bot_id',botIdArray);
					socket.emit('hostname',hostName);
				});

				socket.on('disconnect', function(){
					console.log('user disconnected');
				});
			});
			
			var host = httpsServer.address().address
			var port = httpsServer.address().port

			console.log("UI component listening in secure mode at https://%s:%s", host, port)
		}
		else
		{
			var server = app.listen(sharedProperties.get('appServer.uiSocketPort') , function () {
			var host = server.address().address
			var port = server.address().port
			console.log("UI component listening at http://%s:%s", host, port)
			})
			
			io = require('socket.io')(server);
			io.on('connection', function(socket){
				console.log('a user connected');			
				socket.on('login_msg', function(msg){
					console.log('message: ' + msg);
					console.log("emitted in non-secure mode--------------------"+userName);
					socket.emit('username',userName);
					socket.emit('status',newStatus);
					socket.emit('botAccess',botAccessArray);
				});
				socket.on('disconnect', function(){
					console.log('user disconnected');
				});
			});
		}
			
	}
		
		
} 
module.exports.UIServer = UIServer;	
		
