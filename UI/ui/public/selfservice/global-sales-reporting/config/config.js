

gOptions = {
  // This can have nested stuff, arrays, etc.
  binaryServerUrl:'wss://54.88.108.178:80',
  chatServerTextUrl:'http://localhost:80',
  errorMessage:'Hi, there was an issue connecting with the chat server, please try again in some time',
  audioBufferSize:2048,
  startRecordDelay:1000,
  userName:'',
  botName:'global-sales-reporting-bot'
}