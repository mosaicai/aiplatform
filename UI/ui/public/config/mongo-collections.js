'use strict';

//mondb related properties
mongo-db-host=172.25.33.107
mongo-db-port=27017
mongo-db-name=bots_framework

//bot related properties.
//user detail collection
user-detail-collection=user_details
//login detail collection
login-detail-collection=login_details
//bot-names containing collection
search-detail-collection=bot_information

// bot-name
bot-name=BotName