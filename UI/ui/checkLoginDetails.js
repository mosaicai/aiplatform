'use strict';
var mongodb = require('mongodb');
var MongoClient = mongodb.MongoClient;
var url = 'mongodb://172.25.33.107:27017/bots_framework'; 
var userId=''; 
var resultString = ''; 
var firstName='';
var secondName='';
var botsAccess;
var gender='';
var userFound = '0';
var botArray = [];
var bot_id=[];

class CheckLoginDetails{

	constructor(){
	}

	validateUser(usernamePassed , passwordPassed, callBackFunction)	{

		MongoClient.connect(url, function (err, db) {
			if (err) {
				console.log('Unable to connect to the mongoDB server. Error:', err);
			} else {
				console.log('Connection established to', url);
				var bot_id_collection = db.collection('bot-configuration');
				console.log('Before making mongo call--------');
				bot_id_collection.find({}).toArray(function(err,result){
					console.log('Inside mongo call--------' + result.length);
					if (err) {
						console.log(err);
					}	else if (result.length > 0) {
						result.forEach(function(record) {
							var botRecord = new Object();
							console.log(record._id);
							botRecord.id=record._id;
							botRecord.botname = record.botconfiguration.botname
							botArray.push( JSON.stringify(botRecord) );
						}); 
					}
				});

				var collection = db.collection('user_details');
				
				collection.find({}).toArray(function (err, result) {
					if (err) {
						console.log(err);
					}	else if (result.length > 0) {
						for( let i = 0; i<result.length; i++)	{
							if ( result[i].username == usernamePassed )	{
								userFound = 1;
							}
						}	
						
						if ( userFound == '1' )	{ 
							var collection = db.collection('user_details');
							collection.find({username: usernamePassed}).toArray(function (err, result) {
								if (err) {
									console.log(err);
								}	  else if (result.length) {
									console.log("Document exists");
									console.log('Password:',result[0].password);
									userId = result[0]._id;
									firstName = result[0].firstname;
									secondName = result[0].secondname; 
									botsAccess = result[0]['bots-access'];
									gender=result[0].gender;
									if(result[0].password==passwordPassed&&result[0].isactive=='y'){
										resultString="Success";
										callBackFunction(resultString, userId,usernamePassed,firstName,secondName,botsAccess,gender,botArray);
									}	else	{
										resultString="Failure";  
										callBackFunction(resultString,userId,usernamePassed,firstName,secondName,botsAccess,gender,botArray);
									}	
								} else {
									resultString="Failure";  
									callBackFunction(resultString);
								}
								db.close();
							});	
						}	else	{
							resultString="Failure";  
							callBackFunction(resultString,userId,usernamePassed,firstName,secondName,botsAccess,gender,botArray);
						}	
					} else {
						resultString="Failure";  
						callBackFunction(resultString,userId,usernamePassed,firstName,secondName,botsAccess,gender,botArray);
					}
					db.close();
				});

			}
		});
	}
}

module.exports.CheckLoginDetails = CheckLoginDetails;