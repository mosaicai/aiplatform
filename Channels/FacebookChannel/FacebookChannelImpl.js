'use strict';
var Botkit = require('botkit');
var http = require('http');
var ConfigurationManager = require('../../PlatformAPI/ConfigurationManager/ConfigurationManager.js').ConfigurationManager;
var c_configurationmanager =  new ConfigurationManager();

var c_logger=require('../../PlatformAPI/Utilities/Logger.js')(module);

//Load Entity Status Manager
var EntityStatusManager = require('../../PlatformAPI/EntityStatusManager/EntityStatusManager.js').EntityStatusManager;
var c_entitystatusmanager = new EntityStatusManager();

//Load Entity states enum
var ComponentStatus = require('../../PlatformAPI/Utilities/ComponentStatus.js').ComponentStatus;

//Load Enterprise Entity Logger
var EnterpriseEntityLogger = require('../../PlatformAPI/EnterpriseEntityLogger/EnterpriseEntityLogger.js').EnterpriseEntityLogger;
var c_enterpriseentitylogger = new EnterpriseEntityLogger();


var SessionsUtility = require('../../PlatformAPI/Utilities/SessionsUtility.js').SessionsUtility;

//var bot={};
var c_data;
var c_sessionsutility = {};
var c_userid='';
var c_sessionid='';


let bot={};



class FacebookChannelImpl{

	constructor(p_instancename){
		c_sessionsutility = new SessionsUtility(p_instancename);
		c_logger.info('Channel instantiated for instance:'+p_instancename);
		c_enterpriseentitylogger.insertEntityLog(p_instancename,'FacebookChannel','Channel instantiated for instance:'+p_instancename,'INFO',function(success){
				c_logger.info("Entity log Inserted for (instance:"+p_instancename+", entity:FacebookChannel):"+success);
		});
	}
 	
	//This method loads the channel related configuration
	//Setup the facebook webserver
	//Start listenning for messages from User
	initialiseChannel(p_instancename , l_channelname, channelFactoryCallback){
		try{
			c_logger.info("Entering initialiseChannel method");
			var invokeBotService = function(p_configparammap) {
				c_enterpriseentitylogger.insertEntityLog(p_instancename,l_channelname,'Configuration retrieved from database','INFO',function(success){
					c_logger.info("Entity log Inserted for (instance:"+p_instancename+", entity:"+l_channelname+"):"+success);
				}); 					
				c_logger.info("Entering invokeBotService callback");						
				
				let fb_controller = Botkit.facebookbot({
					access_token:p_configparammap.get('fb-access-token'), // Facebook/Worplace Access Token
					verify_token: p_configparammap.get('http-endpoint-token')		// find the appropriate property
				});
									
				c_entitystatusmanager.updateEntityState(p_instancename,l_channelname,ComponentStatus.STARTING,function(p_statusmessage){
					c_logger.info('Inside updateEntityState callback:'+p_statusmessage);
					bot = fb_controller.spawn({});
					c_enterpriseentitylogger.insertEntityLog(p_instancename,l_channelname,'Starting Facebook channel','ENTITY_STATE',function(success){
						c_logger.info("Entity log Inserted for (instance:"+p_instancename+", entity:"+l_channelname+"):"+success);
					});
					
					c_logger.info('Starting Facebook channel.');															
					c_logger.debug('Inside updateEntityState callback:'+p_statusmessage);											
					try{								
						fb_controller.setupWebserver(parseInt(p_configparammap.get('fb-webserver-port'),10), function(err,webserver) {
							if(err)
							{
								c_logger.error("Error occured :: Class - FacebookChannelImpl :: Method - setupWebserver()");
								//throw new Error('Class - FacebookChannelImpl :: method - setupWebserver() :: Error occured while setting up the webserver');
							}
							else
							{
								fb_controller.createWebhookEndpoints(fb_controller.webserver, bot, function() {						
									c_entitystatusmanager.updateEntityState(p_instancename,l_channelname,ComponentStatus.STARTED,function(){
									
										c_enterpriseentitylogger.insertEntityLog(p_instancename,l_channelname,'Facebook Channel has started','ENTITY_STATE',function(success){
											c_logger.info("Entity log Inserted for (instance:"+p_instancename+", entity:"+l_channelname+"):"+success);
										});
										c_logger.info('Started Facebook channel.');
										c_enterpriseentitylogger.insertEntityLog(p_instancename,l_channelname,'BOT is now online','INFO',function(success){
											c_logger.info("Entity log Inserted for (instance:"+p_instancename+", entity:"+l_channelname+"):"+success);
										});
										c_logger.info('The FB bot is online!!!');
										c_logger.info("Exiting initialiseChannel method");
										channelFactoryCallback('This bot is online!!! Now we can start talking to him');
									});	
								});
							}

						});
					}
					catch(e)
					{
						c_logger.error('Class - FacebookChannelImpl :: method - setupWebserver() :: Error occured while setting up the webserver'+e);
					}
			
				});
			
				//Config Params
				let l_paramnamesfororchestrator = ['orchestrator-host','orchestrator-port'];
				c_configurationmanager.getConfigParamsByListUsingNames(p_instancename, 'BotOrchestrator',l_paramnamesfororchestrator,function(p_configparammapfororhestrator){
					fb_controller.hears([''],'message_received', function(bot, p_messageobject) {										
						c_logger.debug('Received message from facebook in Facebook channel: '+ p_messageobject.text);
						
						
						var sessionsinformation = {
							"sessionscollection" : p_configparammap.get('sessions-collection'),
							"sessionstimeoutinminutes" : p_configparammap.get('session-timeout-in-minutes')
						}
							
						c_logger.debug("sessionsinformation"+JSON.stringify(sessionsinformation));	
						c_userid=p_messageobject.user;							
						c_logger.debug("c_userid::"+c_userid);	
						
						c_sessionsutility.manageUserSession(c_userid,sessionsinformation,c_data,'','','',function(p_sessionId){
							c_logger.debug("Session Id param:"+p_sessionId );
							c_sessionid=p_sessionId;
							c_logger.debug("Session Id retrieved :: "+c_sessionid);	
							
							c_enterpriseentitylogger.insertEntityLog(p_instancename,l_channelname,'Received message from facebook in Facebook  channel: '+ p_messageobject.text,'DEBUG',function(success){
								c_logger.info("Entity log Inserted for (instance:"+p_instancename+", entity:"+l_channelname+"):"+success);
							});
							
							var l_commonobject = {};
							l_commonobject.instancename = p_instancename;
							l_commonobject.user = c_userid;
							l_commonobject.text = p_messageobject.text;
							l_commonobject.channelmessageobject = p_messageobject;
							l_commonobject.sessionid = c_sessionid;
							l_commonobject.responsetext = '';
							
							c_logger.debug("Message object before sending to orchestrator:"+JSON.stringify(l_commonobject));
											
							var processMessageCallOptions = {
								host: p_configparammapfororhestrator.get('orchestrator-host'),
								path: '/BotOrchestrator/processMessage',
								port: p_configparammapfororhestrator.get('orchestrator-port'),
								method:'POST',
								headers: {
									"Content-Type": "application/json",        
								}
							};
							
							let p_commonobject={};
							
							var req = http.request(processMessageCallOptions, function(response){					
								c_logger.debug("Inside callback of orchestrator call");					
								response.on('data', function (chunk) {
									p_commonobject = chunk;
									c_logger.debug("p_commonobject: " + p_commonobject);
								});
								response.on('end', function () {
									c_logger.debug('Received response from orchestrator in Facebook channel: '+ p_commonobject);
									c_enterpriseentitylogger.insertEntityLog(p_instancename,l_channelname,'Received response from orchestrator in Facebook channel: '+ p_commonobject,'DEBUG',function(success){
										c_logger.info("Entity log Inserted for (instance:"+p_instancename+", entity:"+l_channelname+"):"+success);
									});
									let json_commonobject = JSON.parse(p_commonobject);
									if(null != json_commonobject || json_commonobject == '' || undefined!= json_commonobject){
										c_logger.info("Entering callback function: processUserInteraction method");
										bot.reply(json_commonobject.channelmessageobject, json_commonobject.responsetext);
										c_logger.info("Exiting callback function: processUserInteraction method"+p_commonobject.responsetext);
									}
									 else{
										c_logger.error("Error occurred :: Class - FacebookChannelImpl :: Method - processMessageCallback() :: common object not found");
										bot.reply(l_commonobject.channelmessageobject,"There was an issue connecting to the chat server ");
									} 
								});							
							});
							req.end(JSON.stringify(l_commonobject));
						});
						
					});								
				});
			};	
		
			//Config Params
			let l_paramnamesforfb = ['fb-access-token','fb-webserver-port','http-endpoint-token','sessions-collection','session-timeout-in-minutes'];
			
			
			//API call to retreive all the config param values in Map
			c_configurationmanager.getConfigParamsByListUsingNames(p_instancename, l_channelname,l_paramnamesforfb,invokeBotService);
		}
		catch(e)
		{
			c_logger.error("Error occurred :: Class - FacebookChannelImpl :: Method - initialiseChannel() "+e);
		}
	}
}
module.exports = FacebookChannelImpl;
