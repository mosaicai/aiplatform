'use strict';

var fs = require('fs');
var bodyParser = require('body-parser');
var https = require('https');

var util = require('../../PlatformAPI/Utilities/Util.js');
var certsdir = '';

var path = require('path');

//var properties = PropertiesReader(module.filename+'/../../BootStrapProperties.js');
util.trimPath(module.filename,path.sep,2,function(dir){
	certsdir = dir;
});
//var properties = PropertiesReader(bootstrapDir+'/BootStrapProperties.js');

var privateKey  = fs.readFileSync(certsdir+'/AlexaChannel/certs/lnt.key', 'utf8');
var certificate = fs.readFileSync(certsdir+'/AlexaChannel/certs/lnt.crt', 'utf8');
var credentials = {key: privateKey, cert: certificate};
var express = require('express');
var app = express();
var mergeJSON = require("merge-json") ;
var c_logger=require('../../PlatformAPI/Utilities/Logger.js')(module);

var http = require('http');

var ConfigurationManager = require('../../PlatformAPI/ConfigurationManager/ConfigurationManager.js').ConfigurationManager;
var c_configurationmanager =  new ConfigurationManager();

//var CoreOrchestrator = require('../../CoreServices/CoreOrchestrator.js').CoreOrchestrator;


//Load Entity Status Manager
var EntityStatusManager = require('../../PlatformAPI/EntityStatusManager/EntityStatusManager.js').EntityStatusManager;
var l_entitystatusmanager = new EntityStatusManager();
			
//Load Entity states enum
var ComponentStatus = require('../../PlatformAPI/Utilities/ComponentStatus.js').ComponentStatus;

//Load Enterprise Entity Logger
var EnterpriseEntityLogger = require('../../PlatformAPI/EnterpriseEntityLogger/EnterpriseEntityLogger.js').EnterpriseEntityLogger;
var c_enterpriseentitylogger = new EnterpriseEntityLogger();
var c_instancename='';


var SessionsUtility = require('../../PlatformAPI/Utilities/SessionsUtility.js').SessionsUtility;

//var bot={};
var c_data;
var c_sessionsutility = {};
var c_userid='';
var c_sessionid='';


class AlexaChannelImpl{

	constructor(p_instancename){
		c_sessionsutility = new SessionsUtility(p_instancename);
		c_instancename=p_instancename;
	}
	
	initialiseChannel(p_instancename , l_channelname, channelFactoryCallback){
		let l_paramnames = ['alexa-port','alexa-host','sessions-collection','session-timeout-in-minutes','orchestrator-host','orchestrator-port'];
		var l_httpsServer = https.createServer(credentials, app);	
		try{	
			c_configurationmanager.getConfigParamsByListUsingNames(p_instancename, 'AlexaChannel',l_paramnames,function(p_configparammapforchannel){
				var l_alexachannelport=p_configparammapforchannel.get('alexa-port');
				var l_alexachannelhost=p_configparammapforchannel.get('alexa-host');
				var l_server = l_httpsServer.listen(l_alexachannelport,l_alexachannelhost, function () {
				   var host = l_server.address().address
				   var port = l_server.address().port
				   console.log("Example app listening at http://%s:%s", host, port)	
			
				try{
					
					app.use(bodyParser.json()); // for parsing application/json

					
					app.post('/alexachannel', function (req, res, next) {

						console.log("*******************GOT POST****************** :: Inside Alexa Channel"+JSON.stringify(req.body));
						console.log("Request data----->"+JSON.stringify(req.body.result.metadata.intentName));
						var l_intentname = req.body.result.metadata.intentName;
						
						
						c_logger.info("Entering initialiseChannel method");
							var processMessageCallback = function(p_commonobject){
								c_logger.info("Entering callback function: processUserInteraction method"+p_commonobject.responsetext);
								
								try{
									res.send(p_commonobject.responsetext);
								}catch(e){
									c_logger.error("Error while sending response back to Alexa",e);
								}
								
								
								c_logger.info("Exiting callback function: processUserInteraction method");
							}
							
								
							try{
								l_entitystatusmanager.updateEntityState(p_instancename,'AlexaChannel',ComponentStatus.STARTING,function(p_statusmessage){
									c_logger.debug("----------------------------------AlexaChannel starting");
									
									 c_enterpriseentitylogger.insertEntityLog(p_instancename,'AlexaChannel','Starting Slack channel','INFO',function(success){
										});	
									 
									 l_entitystatusmanager.updateEntityState(p_instancename,'AlexaChannel',ComponentStatus.STARTED,function(){
												c_logger.debug("----------------------------------AlexaChannel started");
												c_enterpriseentitylogger.insertEntityLog(p_instancename,'AlexaChannel','Alexa Channel has started','INFO',function(success){
													console.log("Entity log Inserted"+success);
												});
												c_enterpriseentitylogger.insertEntityLog(p_instancename,'AlexaChannel','BOT is now online','INFO',function(success){
													console.log("Entity log Inserted"+success);
												});
												c_logger.info('The FB bot bot is online!!!');
												c_logger.info("Exiting initialiseChannel method");
												channelFactoryCallback('This bot is online!!! Now we can start talking to him');
											});	
								}); 
							}catch(p_error){
								c_logger.error("Error in  AlexaChannelImpl :: updateEntityState",p_error);
							}
							
							
							
							c_logger.info("Received message in alexa channel" +l_intentname);
							c_logger.info("l_intentname============"+l_intentname);
							
						
						
							try{
								c_configurationmanager.getConfigParamsByListUsingNames(p_instancename, 'BotOrchestrator',l_paramnames,function(p_configparammapfororhestrator){
									c_enterpriseentitylogger.insertEntityLog(p_instancename,'AlexaChannel','Received intent in AlexaChannel :: Message :  '+ l_intentname,'DEBUG',function(success){
									c_userid=req.body.result.metadata.userId;
									 var sessionsinformation = {
										"sessionscollection" : p_configparammapforchannel.get('sessions-collection'),
										"sessionstimeoutinminutes" : p_configparammapforchannel.get('session-timeout-in-minutes')
									} 
									
									c_sessionsutility.manageUserSession(c_userid,sessionsinformation,c_data,'','','',function(p_sessionId){
										c_logger.debug("Session Id param:"+p_sessionId );
										c_sessionid=p_sessionId;
										c_logger.debug("Session Id retrieved :: "+c_sessionid);	
									 
										c_logger.debug("Entity log Inserted"+success);
										var l_commonobject = {};
										l_commonobject.instancename = p_instancename;
										l_commonobject.intentrequest=req.body.result.metadata;
										l_commonobject.user = c_userid;
										l_commonobject.text ='';
										l_commonobject.channelmessageobject = '';
										l_commonobject.sessionid = c_sessionid; 
										l_commonobject.responsetext = '';
										
										//bot.reply(p_messageobject,'Welcome');
										
										//responseCallback(l_commonobject);
										//l_coreorchestrator.processUserInteraction(l_commonobject, responseCallback);
										
										
										var processMessageCallOptions = {
											host: p_configparammapfororhestrator.get('orchestrator-host'),
											path: '/BotOrchestrator/processMessage',
											port: p_configparammapfororhestrator.get('orchestrator-port'),
											method:'POST',
											headers: {
												"Content-Type": "application/json",        
											}
										};
												
										let p_commonobject={};
										try{
											var reqobj = http.request(processMessageCallOptions, function(response){					
												c_logger.debug("Inside callback");					
												response.on('data', function (chunk) {
													p_commonobject = chunk;
													c_logger.debug("p_commonobject: " + p_commonobject);
												});
												response.on('end', function () {			
													processMessageCallback(JSON.parse(p_commonobject));
												});							
											});
											reqobj.end(JSON.stringify(l_commonobject));
										}catch(p_err){
											c_logger.error("Error in AlexaChannelImpl while making rest call to BotOrchestrator",p_err);
										}

										});
									}); 
						
								});	
							}catch(p_err){
								c_logger.error("Error in AlexaChannelImpl :: getConfigParamsByListUsingNames",p_err);
							}	
							
							
					
						});
				
			
				}catch(p_error){
					c_logger.error("Error in Class- AlexaChannelImpl :: method - initialiseChannel ",p_error);
				}
					});
			});
		}catch(p_error){
			c_logger.error("Error in Class- AlexaChannelImpl :: method - initialiseChannel ",p_error);
		
		}
		
	}
	
}

	
	
	
	module.exports=AlexaChannelImpl;