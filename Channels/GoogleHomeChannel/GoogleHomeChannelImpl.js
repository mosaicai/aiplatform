'use strict';

var fs = require('fs');
var bodyParser = require('body-parser');
var https = require('https');



var util = require('../../PlatformAPI/Utilities/Util.js');
var certsdir = '';

var path = require('path');

//var properties = PropertiesReader(module.filename+'/../../BootStrapProperties.js');
util.trimPath(module.filename,path.sep,2,function(dir){
	certsdir = dir;
});

var privateKey  = fs.readFileSync(certsdir+'/GoogleHomeChannel/certs/lnt.key', 'utf8');
var certificate = fs.readFileSync(certsdir+'/GoogleHomeChannel/certs/lnt.crt', 'utf8');
var credentials = {key: privateKey, cert: certificate};
var express = require('express');
var app = express();
var mergeJSON = require("merge-json") ;
var c_logger=require('../../PlatformAPI/Utilities/Logger.js')(module);

var ConfigurationManager = require('../../PlatformAPI/ConfigurationManager/ConfigurationManager.js').ConfigurationManager;
var c_configurationmanager =  new ConfigurationManager();

//var CoreOrchestrator = require('../../CoreServices/CoreOrchestrator.js').CoreOrchestrator;


var http = require('http');

//Load Entity Status Manager
var EntityStatusManager = require('../../PlatformAPI/EntityStatusManager/EntityStatusManager.js').EntityStatusManager;
var l_entitystatusmanager = new EntityStatusManager();
			
//Load Entity states enum
var ComponentStatus = require('../../PlatformAPI/Utilities/ComponentStatus.js').ComponentStatus;

//Load Enterprise Entity Logger
var EnterpriseEntityLogger = require('../../PlatformAPI/EnterpriseEntityLogger/EnterpriseEntityLogger.js').EnterpriseEntityLogger;
var c_enterpriseentitylogger = new EnterpriseEntityLogger();
var c_instancename='';


var SessionsUtility = require('../../PlatformAPI/Utilities/SessionsUtility.js').SessionsUtility;
var c_sessionsutility = {};


class GoogleHomeChannelImpl{
	constructor(p_instancename){
		c_sessionsutility = new SessionsUtility(p_instancename);
	}
	initialiseChannel(p_instancename , l_channelname, channelFactoryCallback){
		
		app.use(bodyParser.json()); // for parsing application/json

		var httpsServer = https.createServer(credentials, app);
		let c_data='';
		let c_sessionid='';
		let c_userid='';
		let l_paramnames = ['googlehome-port','googlehome-host','sessions-collection','session-timeout-in-minutes','orchestrator-host','orchestrator-port'];
		
		try{
			c_configurationmanager.getConfigParamsByListUsingNames(p_instancename, 'GoogleHomeChannel',l_paramnames,function(p_configparammapforchannel){
				var l_googlehomeport=p_configparammapforchannel.get('googlehome-port');
				var l_googlehomehost=p_configparammapforchannel.get('googlehome-host');
					
				var server = httpsServer.listen(l_googlehomeport,l_googlehomehost, function () {
				   var host = server.address().address
				   var port = server.address().port
				   console.log("Example app listening at http://%s:%s", host, port)
				});
			
		
			app.post('/googleHomechannel', function (req, res, next) {

				console.log("*******************GOT POST****************** :: "+JSON.stringify(req.body));
				console.log("Request data----->"+JSON.stringify(req.body.result.metadata.intentName));
				
				var l_intentname = req.body.result.metadata.intentName;
				
				
				
				c_logger.info("Entering initialiseChannel method");
					var processMessageCallback = function(p_commonobject){
						var l_responseobj ={
							speech:p_commonobject.responsetext
						}
						c_logger.info("Entering callback function: processUserInteraction method"+p_commonobject.responsetext);
						
							
						try{
							res.send(l_responseobj);
						}catch(e){
							c_logger.error("Error while sending response back to Google Home",e);
						}
					
						c_logger.info("Exiting callback function: processUserInteraction method");
					}
					
				
					c_logger.info("Received message in google home channel" +l_intentname);
					c_logger.info("google home intent name============"+l_intentname);
					
					
				
					try{
						c_configurationmanager.getConfigParamsByListUsingNames(p_instancename, 'BotOrchestrator',l_paramnames,function(p_configparammapfororhestrator){
						
						
							c_enterpriseentitylogger.insertEntityLog(p_instancename,'GoogleHomeChannel','Received intent in Google Home channel :: Message :  '+ l_intentname,'DEBUG',function(success){
								c_logger.debug("Entity log Inserted"+success);
								
								if(req.body.originalRequest !=null || req.body.originalRequest!='' && req.body.originalRequest!=undefined){
									c_userid=req.body.originalRequest.data.user.user_id;
								}else{
									c_userid=req.body.sessionId;
								}
								
							
								
								var sessionsinformation = {
									"sessionscollection" : p_configparammapforchannel.get('sessions-collection'),
									"sessionstimeoutinminutes" : p_configparammapforchannel.get('session-timeout-in-minutes')
								}
							
							c_sessionsutility.manageUserSession(c_userid,sessionsinformation,c_data,'','','',function(p_sessionId){
								c_logger.debug("Session Id param:"+p_sessionId );
								c_sessionid=p_sessionId;
								c_logger.debug("Session Id retrieved :: "+c_sessionid);		
							

								var l_commonobject = {};
								l_commonobject.instancename = p_instancename;
								l_commonobject.user = c_userid;
								//l_commonobject.text =req.body.result.resolvedQuery;
								l_commonobject.text ='';
								l_commonobject.channelmessageobject = '';
								l_commonobject.sessionid = c_sessionid; 
								l_commonobject.responsetext = '';
								//l_commonobject.bot=bot;
								l_commonobject.intentrequest=req.body.result;
								//bot.reply(p_messageobject,'Welcome');
								c_logger.debug("Forming the common object :: "+l_commonobject);
								//responseCallback(l_commonobject);
								c_logger.debug("Before calling process user interaction");
								//l_coreorchestrator.processUserInteraction(l_commonobject, responseCallback);
								
								var processMessageCallOptions = {
									host: p_configparammapfororhestrator.get('orchestrator-host'),
									path: '/BotOrchestrator/processMessage',
									port: p_configparammapfororhestrator.get('orchestrator-port'),
									method:'POST',
									headers: {
										"Content-Type": "application/json",        
									}
								};
								
									let p_commonobject={};
								try{
									var reqobj = http.request(processMessageCallOptions, function(response){					
										c_logger.debug("Inside callback");					
										response.on('data', function (chunk) {
											p_commonobject = chunk;
											c_logger.debug("p_commonobject: " + p_commonobject);
										});
										response.on('end', function () {			
											processMessageCallback(JSON.parse(p_commonobject));
										});							
									});
									reqobj.end(JSON.stringify(l_commonobject));
									
								}catch(p_err){
									c_logger.error("Error in GoogleHomeChannelImpl while making rest call to BotOrchestrator",p_err);
								}
								
								
								});
							}); 
						});
					}catch(p_err){
						c_logger.error("Error in GoogleHomeChannelImpl :: getConfigParamsByListUsingNames",p_err);
					}	
					
					
			});
			
				});
		}catch(p_error){
			c_logger.error("Error in AlexaChannelImpl :: getConfigParamsByListUsingNames() ",p_error);
		}
			
				
			
	}
}


	
	
	
	
	module.exports=GoogleHomeChannelImpl;