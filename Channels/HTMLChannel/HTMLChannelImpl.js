'use strict';
var ConfigurationManager = require('../../PlatformAPI/ConfigurationManager/ConfigurationManager.js').ConfigurationManager;
var c_configurationmanager =  new ConfigurationManager();

var http = require('http');

//var CoreOrchestrator = require('../../CoreServices/CoreOrchestrator.js').CoreOrchestrator;
var c_logger=require('../../PlatformAPI/Utilities/Logger.js')(module);

//Load Entity Status Manager
var EntityStatusManager = require('../../PlatformAPI/EntityStatusManager/EntityStatusManager.js').EntityStatusManager;
var c_entitystatusmanager = new EntityStatusManager();

//Load Entity states enum
var ComponentStatus = require('../../PlatformAPI/Utilities/ComponentStatus.js').ComponentStatus;
var SessionsUtility = require('../../PlatformAPI/Utilities/SessionsUtility.js').SessionsUtility;
var c_sessionsutility = {};
var c_roomid;
var c_botid;  
var c_userid;
var c_sessionid;
var c_data;

var c_express = require('express');
var c_app = c_express();
var c_https = require('https');
var fs = require('fs');

//Load Enterprise Entity Logger
var EnterpriseEntityLogger = require('../../PlatformAPI/EnterpriseEntityLogger/EnterpriseEntityLogger.js').EnterpriseEntityLogger;
var c_enterpriseentitylogger = new EnterpriseEntityLogger();

// Instance of HTML channel adapter
class HTMLChannelImpl{
	
	constructor(p_instancename){
		c_sessionsutility = new SessionsUtility(p_instancename);
		c_logger.info('channel instantiated for instance:'+p_instancename);
		c_enterpriseentitylogger.insertEntityLog(p_instancename,'HTMLChannel','Channel instantiated for instance:'+p_instancename,'INFO',function(success){
				c_logger.info("Entity log Inserted for (instance:"+p_instancename+", entity:HTMLChannel):"+success);
		});
	}
			
	//This method loads the channel related configuration
	//Setup the facebook webserver
	//Start listenning for messages from User
	initialiseChannel(p_instancename , l_channelname, channelFactoryCallback){
		try{
			c_logger.info("Entering initialiseChannel method");
			var invokeBotService = function(p_configparammap) {
				c_enterpriseentitylogger.insertEntityLog(p_instancename,l_channelname,'Configuration retrieved from database','INFO',function(success){
					c_logger.info("Entity log Inserted for (instance:"+p_instancename+", entity:"+l_channelname+"):"+success);
				}); 					
				
				c_logger.info("Entering invokeBotService callback");											
				c_logger.info("---------------WEB SOCKET PORT----------------"+p_configparammap.get('web-socket-port'));
				
				var io;
				c_entitystatusmanager.updateEntityState(p_instancename,l_channelname,ComponentStatus.STARTING,function(p_statusmessage){
				
					c_enterpriseentitylogger.insertEntityLog(p_instancename,l_channelname,'Starting HTML channel','ENTITY_STATE',function(success){
						c_logger.info("Entity log Inserted for (instance:"+p_instancename+", entity:"+l_channelname+"):"+success);
					});
					c_logger.info('Starting HTML channel.');
					
					try{
						if(p_configparammap.get('mode') === 'secure'){								
							let l_webAppsPort=p_configparammap.get('web-socket-port');
							let httpsServer = c_https.createServer({
								key: fs.readFileSync(__dirname+'/../../UI/certs/key.pem'),
								cert: fs.readFileSync(__dirname+'/../../UI/certs/cert.pem')
							},c_app).listen(l_webAppsPort);
							
							io = require('socket.io')(httpsServer);
							var l_host = httpsServer.address().address;
							var l_port = httpsServer.address().port;

							c_logger.info('HTMLChannel listening in secure mode at https://'+l_host+':'+l_port);
							c_enterpriseentitylogger.insertEntityLog(p_instancename,l_channelname,'HTMLChannel listening in secure mode at https://'+l_host+':'+l_port,'ENTITY_STATE',function(success){
								c_logger.info("Entity log Inserted for (instance:"+p_instancename+", entity:"+l_channelname+"):"+success);
							}); 	
							
						}
						else{
						
							c_logger.info('Inside non-secure');
							var l_server = c_app.listen(p_configparammap.get('web-socket-port') , function () {				
								var l_host = l_server.address().address;
								var l_port = l_server.address().port;
								c_logger.info('HTMLChannel listening in non-secure mode at https://'+l_host+':'+l_port);
							});							
							io = require('socket.io')(l_server);								
						}
					}
					catch(error){						
						c_logger.error("Error occurred while initialiseChannel :: Class - HTMLChannelImpl :: "+error);
						throw new Error("Error occurred while initialiseChannel :: Class - HTMLChannelImpl :: "+error);
					}						
						
					c_entitystatusmanager.updateEntityState(p_instancename,l_channelname,ComponentStatus.STARTED,function(){
						c_enterpriseentitylogger.insertEntityLog(p_instancename,l_channelname,'HTMLChannel Started','INFO',function(success){
							c_logger.info("Entity log Inserted for (instance:"+p_instancename+", entity:"+l_channelname+"):"+success);
						});
						c_logger.info('Started HTML channel.');
						c_enterpriseentitylogger.insertEntityLog(p_instancename,l_channelname,'BOT is now online','INFO',function(success){
							c_logger.info("Entity log Inserted for (instance:"+p_instancename+", entity:"+l_channelname+"):"+success);
						});
						c_logger.info('The HTML bot is online!!!');
						c_logger.info("Exiting initialiseChannel method");
						channelFactoryCallback('This bot is online!!! Now we can start talking to him');
					});					
						
					io.on('connection', function(socket){
						
						c_logger.debug('Inside io.on connection socket:: '+socket);
						c_enterpriseentitylogger.insertEntityLog(p_instancename,l_channelname,'Inside io.on connection socket:: '+socket,'DEBUG',function(success){
							c_logger.info("Entity log Inserted for (instance:"+p_instancename+", entity:"+l_channelname+"):"+success);
						});
						
						socket.on('login_msg', function(msg){
							c_logger.debug('Inside login_msg event, socket: '+socket);
							socket.emit('mode',p_configparammap.get('mode'));
						});
						c_logger.debug('chatserver connects AI Jarvis, Connection id: ' + socket.client.conn.id);  				 
						
						c_logger.info("Connection done------------------");				
						
						 var thisSocket;
						//let roomId='';  
						
						let ip = socket.handshake.headers.host;
										
						socket.on('room', function(p_room) {
							socket.join(p_room);
							c_logger.debug('Client joins with roomID ' + p_room);
							c_enterpriseentitylogger.insertEntityLog(p_instancename,l_channelname,'Client joins with roomID ' + p_room,'DEBUG',function(success){
								c_logger.info("Entity log Inserted for (instance:"+p_instancename+", entity:"+l_channelname+"):"+success);
							});
							
							var values = p_room.split('-');			
							c_botid = values[0];
							c_userid = values[1];
							c_logger.debug("c_botid-------------->"+c_botid);
																					
							c_roomid = p_room;

						});
						
						
						let l_paramnamesfororchestrator = ['orchestrator-host','orchestrator-port'];
						c_configurationmanager.getConfigParamsByListUsingNames(p_instancename, 'BotOrchestrator',l_paramnamesfororchestrator,function(p_configparammapfororhestrator){
							// Hear to the incoming connection
							socket.on('input-to-jarvis',function(p_messageobject){ 
								c_logger.debug("Received message in HTML channel:"+p_messageobject.text );
								c_enterpriseentitylogger.insertEntityLog(p_instancename,l_channelname,'Received message in HTML channel: '+ p_messageobject.text,'DEBUG',function(success){
									c_logger.info("Entity log Inserted for (instance:"+p_instancename+", entity:"+l_channelname+"):"+success);
								});
								
								c_logger.info('--------------------Message recieved----------------------');						
								
								var processMessageCallback = function(p_commonobject){
									if(null != p_commonobject || p_commonobject == '' || undefined!= p_commonobject){
										c_logger.debug(p_commonobject.responsetext);
										c_data = p_commonobject.responsetext;
										socket.emit("output-from-jarvis", {data:p_commonobject.responsetext,room:p_messageobject.user});
									}
									else{
										c_logger.error("Error occurred :: Class - HTMLChannelImpl in orchestrator response :: p_commonobject not found");
										l_commonobject.responsetext = 'There was an issue connecting with the chat server. Please try agin after some time.';
										socket.emit("output-from-jarvis", {data:l_commonobject.responsetext,room:p_messageobject.user});
									}
								};
								var sessionsinformation = {
									"sessionscollection" : p_configparammap.get('sessions-collection'),
									"sessionstimeoutinminutes" : p_configparammap.get('session-timeout-in-minutes')
								}
								
								var l_commonobject = {};
								//manageUserSession( userId, sessionsinformation, data, medium, userInteraction, channelAdaptor)
								c_sessionsutility.manageUserSession(c_userid,sessionsinformation,c_data,'','','',function(p_sessionId){
									c_logger.debug("Session Id param:"+p_sessionId );
									c_sessionid=p_sessionId;
									c_logger.debug("Session Id retrieved :: "+c_sessionid);
																																	
									l_commonobject.instancename = p_instancename;
									l_commonobject.user = c_userid;
									l_commonobject.text = p_messageobject;
									l_commonobject.channelmessageobject = p_messageobject;
									l_commonobject.sessionid = c_sessionid;
									l_commonobject.responsetext = '';
									
									
									c_logger.debug("Message object before sending to orchestrator:"+JSON.stringify(l_commonobject));							
									var processMessageCallOptions = {
										host: p_configparammapfororhestrator.get('orchestrator-host'),
										path: '/BotOrchestrator/processMessage',
										port: p_configparammapfororhestrator.get('orchestrator-port'),
										method:'POST',
										headers: {
											"Content-Type": "application/json",        
										}
									};
										
									let p_commonobject={};
									try{
										var req = http.request(processMessageCallOptions, function(response){					
											c_logger.debug("Inside callback of orchestrator call");					
											response.on('data', function (chunk) {
												p_commonobject = chunk;
												c_logger.debug("p_commonobject: " + p_commonobject);
											});
											response.on('end', function () {
												c_logger.debug('Received response from orchestrator in HTML channel: '+ p_commonobject);
												c_enterpriseentitylogger.insertEntityLog(p_instancename,l_channelname,'Received response from orchestrator in HTML channel: '+ p_commonobject,'DEBUG',function(success){
													c_logger.info("Entity log Inserted for (instance:"+p_instancename+", entity:"+l_channelname+"):"+success);
												});									
												processMessageCallback(JSON.parse(p_commonobject));
											});							
										});
										req.end(JSON.stringify(l_commonobject));
									}
									catch(error){
										c_logger.error("Error occurred while orchestrator call :: Class - HTMLChannelImpl :: "+error);
										l_commonobject.responsetext = 'There was an issue connecting with the chat server. Please try agin after some time.';
										socket.emit("output-from-jarvis", {data:l_commonobject.responsetext,room:p_messageobject.user});
									}
									
								});
							});								
								
						});
							
					});	

					io.on('disconnect', function(){
						c_logger.info('User disconnected');					
						c_enterpriseentitylogger.insertEntityLog(p_instancename,l_channelname,'User disconnected','DEBUG',function(success){
							c_logger.info("Entity log Inserted for (instance:"+p_instancename+", entity:"+l_channelname+"):"+success);
						});
					});
				});
			};
				
			let l_paramnames = ['web-socket-port','sessions-collection','session-timeout-in-minutes','mode'];				
			c_configurationmanager.getConfigParamsByListUsingNames(p_instancename, l_channelname,l_paramnames,invokeBotService);
		}
		catch(error){
			c_logger.error("Error occurred while initialiseChannel :: Class - HTMLChannelImpl :: "+error);
			throw new Error("Error occurred while initialiseChannel :: Class - HTMLChannelImpl :: "+error);
		}
	}		
}	
module.exports = HTMLChannelImpl;