'use strict';
var Botkit = require('botkit');
var http = require('http');
var ConfigurationManager = require('../../PlatformAPI/ConfigurationManager/ConfigurationManager.js').ConfigurationManager;
var c_configurationmanager =  new ConfigurationManager();

//var BotOrchestrator = require('../../CoreServices/BotOrchestrator.js').BotOrchestrator;
var c_logger=require('../../PlatformAPI/Utilities/Logger.js')(module);

//Load Entity Status Manager
var EntityStatusManager = require('../../PlatformAPI/EntityStatusManager/EntityStatusManager.js').EntityStatusManager;
var c_entitystatusmanager = new EntityStatusManager();

//Load Entity states enum
var ComponentStatus = require('../../PlatformAPI/Utilities/ComponentStatus.js').ComponentStatus;

//Load Enterprise Entity Logger
var EnterpriseEntityLogger = require('../../PlatformAPI/EnterpriseEntityLogger/EnterpriseEntityLogger.js').EnterpriseEntityLogger;
var c_enterpriseentitylogger = new EnterpriseEntityLogger();

var SessionsUtility = require('../../PlatformAPI/Utilities/SessionsUtility.js').SessionsUtility;

//var bot={};
var c_data;
var c_sessionsutility = {};
var c_userid='';
var c_sessionid='';

class SlackChannelImpl{

	constructor(p_instancename){
	
		c_sessionsutility = new SessionsUtility(p_instancename);
		c_logger.info('channel instantiated for instance:'+p_instancename);
		c_enterpriseentitylogger.insertEntityLog(p_instancename,'SlackChannel','Channel instantiated for instance:'+p_instancename,'INFO',function(success){
				c_logger.info("Entity log Inserted for (instance:"+p_instancename+", entity:SlackChannel):"+success);
		});
	}

	initialiseChannel(p_instancename , l_channelname, channelFactoryCallback){
		try{
			c_logger.info("Entering initialiseChannel method");		
			
			var invokeBotService = function(p_configparammap) {
				c_enterpriseentitylogger.insertEntityLog(p_instancename,'SlackChannel','Configuration retrieved from database','INFO',function(success){
					c_logger.info("Entity log Inserted for (instance:"+p_instancename+", entity:"+l_channelname+"):"+success);
				}); 
				
				c_logger.info("Entering invokeBotService callback");				
				
				c_entitystatusmanager.updateEntityState(p_instancename,l_channelname,ComponentStatus.STARTING,function(p_statusmessage){
					c_logger.info('Inside updateEntityState callback:'+p_statusmessage);				
					
					c_enterpriseentitylogger.insertEntityLog(p_instancename,l_channelname,'Starting Slack channel','ENTITY_STATE',function(success){
						c_logger.info("Entity log Inserted for (instance:"+p_instancename+", entity:"+l_channelname+"):"+success);
					});
					c_logger.info('Starting Slack channel.');
					
					let l_slack_token = p_configparammap.get('slack-token');								
					c_logger.debug("Invoking slack hears...." +l_slack_token);	

					var l_slack_controller = Botkit.slackbot();
					// connect the bot to a stream of messages and invoke it using a token
					try{
						l_slack_controller.spawn({
							token: l_slack_token 
						}).startRTM();				
					}
					catch(error){
						c_logger.error("Error occurred while initialiseChannel :: Class - SlackChannelImpl :: "+error);
						throw new Error("Error occurred while initialiseChannel :: Class - SlackChannelImpl :: "+error);
					}
					
					c_entitystatusmanager.updateEntityState(p_instancename,l_channelname,ComponentStatus.STARTED,function(){						
						c_enterpriseentitylogger.insertEntityLog(p_instancename,l_channelname,'Slack Channel has started','ENTITY_STATE',function(success){
							c_logger.info("Entity log Inserted for (instance:"+p_instancename+", entity:"+l_channelname+"):"+success);
						});
						c_logger.info('Started Slack channel.');
						c_enterpriseentitylogger.insertEntityLog(p_instancename,l_channelname,'BOT is now online','INFO',function(success){
							c_logger.info("Entity log Inserted for (instance:"+p_instancename+", entity:"+l_channelname+"):"+success);
						});
						c_logger.info('The Slack bot bot is online!!!');
						c_logger.info("Exiting initialiseChannel method");
						channelFactoryCallback('This bot is online!!! Now we can start talking to him');
					});
					
				
					//Config Params
					let l_paramnamesfororchestrator = ['orchestrator-host','orchestrator-port'];
					c_configurationmanager.getConfigParamsByListUsingNames(p_instancename, 'BotOrchestrator',l_paramnamesfororchestrator,function(p_configparammapfororhestrator){
				
						l_slack_controller.hears('.*', ['direct_message','direct_mention','mention'] ,function(bot,p_messageobject) {
								
							c_logger.debug("p_configparammap"+JSON.stringify(p_configparammap));
							var sessionsinformation = {
								"sessionscollection" : p_configparammap.get('sessions-collection'),
								"sessionstimeoutinminutes" : p_configparammap.get('session-timeout-in-minutes')
							}
							
							c_logger.debug("sessionsinformation"+JSON.stringify(sessionsinformation));	
							c_userid=p_messageobject.user;							
							c_logger.debug("c_userid::"+c_userid);	
						
							c_sessionsutility.manageUserSession(c_userid,sessionsinformation,c_data,'','','',function(p_sessionId){
								c_logger.debug("Session Id param:"+p_sessionId );
								c_sessionid=p_sessionId;
								c_logger.debug("Session Id retrieved :: "+c_sessionid);	
								
								c_logger.debug('Received message from facebook in Facebook channel: '+ p_messageobject.text);
								
								c_enterpriseentitylogger.insertEntityLog(p_instancename,'SlackChannel','Received message from slack in Slack channel: '+ p_messageobject.text,'DEBUG',function(success){
									c_logger.debug("Entity log Inserted:"+success);
								});
								var l_commonobject = {};
								l_commonobject.instancename = p_instancename;
								l_commonobject.user = c_userid;
								l_commonobject.text = p_messageobject.text;
								l_commonobject.channelmessageobject = p_messageobject;
								l_commonobject.sessionid = c_sessionid;
								l_commonobject.responsetext = '';
								
								c_logger.debug("Message object before sending to orchestrator:"+JSON.stringify(l_commonobject));
								
								var processMessageCallOptions = {
									host: p_configparammapfororhestrator.get('orchestrator-host'),
									path: '/BotOrchestrator/processMessage',
									port: p_configparammapfororhestrator.get('orchestrator-port'),
									method:'POST',
									headers: {
										"Content-Type": "application/json",        
									}
								};
								
								let p_commonobject={};
								try{
									var req = http.request(processMessageCallOptions, function(response){					
										c_logger.debug("Inside callback of orchestrator call");					
										response.on('data', function (chunk) {
											p_commonobject = chunk;
											c_logger.debug("p_commonobject: " + p_commonobject);
										});
										response.on('end', function () {															
											c_logger.info("Entering callback function: processUserInteraction method");								
											c_logger.debug('Received response from orchestrator in Slack channel: '+ p_commonobject);
											c_enterpriseentitylogger.insertEntityLog(p_instancename,l_channelname,'Received response from orchestrator in Slack channel: '+ p_commonobject,'DEBUG',function(success){
												c_logger.info("Entity log Inserted for (instance:"+p_instancename+", entity:"+l_channelname+"):"+success);
											});
											if(null != p_commonobject || p_commonobject == '' || undefined!= p_commonobject){
												let response_commonobject = JSON.parse(p_commonobject);
												bot.reply(response_commonobject.channelmessageobject, response_commonobject.responsetext);
												c_logger.info("Exiting callback function: processUserInteraction method");
											}
											else{
												c_logger.error("Error occurred :: Class - SlackChannelImpl in orchestrator response :: p_commonobject not found");
												bot.reply(l_commonobject.channelmessageobject, 'There was an issue connecting with the chat server. Please try agin after some time.');
											}
										});
										response.on('error', function (error) {															
											c_logger.error("Error occurred :: Class - SlackChannelImpl in orchestrator response :: "+error);
										});		
									});						
									req.end(JSON.stringify(l_commonobject));
							}
							catch(error){
								c_logger.error("Error occurred while orchestrator call :: Class - SlackChannelImpl :: "+error);
								bot.reply(l_commonobject.channelmessageobject, 'There was an issue connecting with the chat server. Please try agin after some time.');
							}
						});
						});								
					});
				});				
			};
		
			let l_paramnames = ['slack-token','sessions-collection','session-timeout-in-minutes'];
			//Change needed : Instead of list method should return a map with key and value
			c_configurationmanager.getConfigParamsByListUsingNames(p_instancename, l_channelname,l_paramnames,invokeBotService);
		}
		catch(error){
			c_logger.error("Error occurred while initialiseChannel :: Class - SlackChannelImpl :: "+error);
			throw new Error("Error occurred while initialiseChannel :: Class - SlackChannelImpl :: "+error);
		}
	}
}


module.exports = SlackChannelImpl;