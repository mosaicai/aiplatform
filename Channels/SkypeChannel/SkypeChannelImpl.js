'use strict';

var restify = require('restify');
var builder = require('botbuilder');


var ConfigurationManager = require('../../PlatformAPI/ConfigurationManager/ConfigurationManager.js').ConfigurationManager;
var c_configurationmanager =  new ConfigurationManager();

//var BotOrchestrator = require('../../CoreServices/BotOrchestrator.js').BotOrchestrator;
var c_logger=require('../../PlatformAPI/Utilities/Logger.js')(module);

//Load Entity Status Manager
var EntityStatusManager = require('../../PlatformAPI/EntityStatusManager/EntityStatusManager.js').EntityStatusManager;

//Load Entity states enum
var ComponentStatus = require('../../PlatformAPI/Utilities/ComponentStatus.js').ComponentStatus;
var http = require('http');
//Load Enterprise Entity Logger
var EnterpriseEntityLogger = require('../../PlatformAPI/EnterpriseEntityLogger/EnterpriseEntityLogger.js').EnterpriseEntityLogger;
var c_enterpriseentitylogger = new EnterpriseEntityLogger();

var bot={};
var SessionsUtility = require('../../PlatformAPI/Utilities/SessionsUtility.js').SessionsUtility;

//var bot={};
var c_data;
var c_sessionsutility = {};
var c_userid='';
var c_sessionid='';

class SkypeChannelImpl{

	constructor(p_instancename){
		c_logger.info('channel instantiated for instance:'+p_instancename);	
		c_sessionsutility = new SessionsUtility(p_instancename);
		c_enterpriseentitylogger.insertEntityLog(p_instancename,'SkypeChannel','Channel instantiated for instance:'+p_instancename,'INFO',function(success){
				c_logger.info("Entity log Inserted for (instance:"+p_instancename+", entity:SkypeChannel):"+success);
		});
	}
	
	
	//This method loads the channel related configuration
	//Setup the Skype webserver
	//Start listenning for messages from User
	initialiseChannel(p_instancename , l_channelname, channelFactoryCallback){
		try{
		
			c_logger.info("Entering initialiseChannel method");
			var invokeBotService = function(p_configparammap) {
			
				c_enterpriseentitylogger.insertEntityLog(p_instancename,l_channelname,'Configuration retrieved from database','INFO',function(success){
				c_logger.info("Entity log Inserted for (instance:"+p_instancename+", entity:"+l_channelname+"):"+success);
				}); 
						
				c_logger.info("Entering invokeBotService callback");
				var processMessageCallback = function(p_commonobject){
					c_logger.info("Entering callback function: processUserInteraction method");
					let l_messageobject = p_commonobject.channelmessageobject;
					l_messageobject.send(p_commonobject.responsetext);
					c_logger.info("Exiting callback function: processUserInteraction method");
				}
				
				var l_entitystatusmanager = new EntityStatusManager();
				//var l_botorchestrator =  new BotOrchestrator();
				l_entitystatusmanager.updateEntityState(p_instancename,'SkypeChannel',ComponentStatus.STARTING,function(p_statusmessage){	
				
					c_enterpriseentitylogger.insertEntityLog(p_instancename,'SkypeChannel','Starting Skype channel','INFO',function(success){
					});
					c_logger.info('Starting Skype channel.');			
					
					//l_botorchestrator.initialiseCoreServices(p_instancename, coreServicesCallback);
							
					let connector = new builder.ChatConnector({
						appId: p_configparammap.get('skype-app-id'),
						appPassword: p_configparammap.get('skype-app-password')
					});

					bot = new builder.UniversalBot(connector);

					var server = restify.createServer();
					server.listen(process.env.port || process.env.PORT || p_configparammap.get('skype-webserver-port'), function ()	{		
						c_logger.info('%s listening to %s', server.name, server.url);
					});						
					
					server.post('/api/messages', connector.listen());
					
					l_entitystatusmanager.updateEntityState(p_instancename,l_channelname,ComponentStatus.STARTED,function(){						
						c_enterpriseentitylogger.insertEntityLog(p_instancename,l_channelname,'Skype Channel has started','ENTITY_STATE',function(success){
							c_logger.info("Entity log Inserted for (instance:"+p_instancename+", entity:"+l_channelname+"):"+success);
						});
						c_logger.info('Started Skype channel.');
						c_enterpriseentitylogger.insertEntityLog(p_instancename,l_channelname,'BOT is now online','INFO',function(success){
							c_logger.info("Entity log Inserted for (instance:"+p_instancename+", entity:"+l_channelname+"):"+success);
						});
						c_logger.info('The Skype bot bot is online!!!');
						c_logger.info("Exiting initialiseChannel method");
						channelFactoryCallback('This bot is online!!! Now we can start talking to him');
					});		
					
					
					//Config Params
					let l_paramnamesfororchestrator = ['orchestrator-host','orchestrator-port'];
					
					try{
						c_configurationmanager.getConfigParamsByListUsingNames(p_instancename, 'BotOrchestrator',l_paramnamesfororchestrator,function(p_configparammapfororhestrator){
							bot.dialog('/', function (p_messageobject) {
								c_logger.debug("p_configparammap"+JSON.stringify(p_configparammap));
								c_logger.debug(p_configparammap.get('sessions-collection'));
								c_logger.debug(p_configparammap.get('session-timeout-in-minutes'));
							var sessionsinformation = {
								"sessionscollection" : p_configparammap.get('sessions-collection'),
								"sessionstimeoutinminutes" : p_configparammap.get('session-timeout-in-minutes')
							}
							
							c_logger.debug("sessionsinformation"+JSON.stringify(sessionsinformation));	
							c_userid=p_messageobject.message.user.id;							
							c_logger.debug("c_userid::"+c_userid);	
						
							c_sessionsutility.manageUserSession(c_userid,sessionsinformation,c_data,'','','',function(p_sessionId){
								c_logger.debug("Session Id param:"+p_sessionId );
								c_sessionid=p_sessionId;
								c_logger.debug("Session Id retrieved :: "+c_sessionid);
								c_logger.info("Received message in Skype channel");
								c_logger.info("message============"+p_messageobject.message.text);
								
								c_enterpriseentitylogger.insertEntityLog(p_instancename,'SkypeChannel','Received message in Skype channel :: Message :  '+ p_messageobject.text,'DEBUG',function(success){
									c_logger.debug("Entity log Inserted:"+success);
								});
								var l_commonobject = {};
								l_commonobject.instancename = p_instancename;
								l_commonobject.user = p_messageobject.message.user.id;
								l_commonobject.text = p_messageobject.message.text;;
								//l_commonobject.channelmessageobject = p_messageobject;
								l_commonobject.sessionid = c_sessionid;
								l_commonobject.responsetext = '';
								//c_logger.debug("User:  +++++++++++++++++++++++++++++++++++++++"+l_commonobject.user);
								//l_botorchestrator.processUserInteraction(l_commonobject, processMessageCallback);
								//p_messageobject.send('Welcome to my app');
								
								var processMessageCallOptions = {
									host: p_configparammapfororhestrator.get('orchestrator-host'),
									path: '/BotOrchestrator/processMessage',
									port: p_configparammapfororhestrator.get('orchestrator-port'),
									method:'POST',
									headers: {
										"Content-Type": "application/json",        
									}
								};
								
								let p_commonobject={};
								try{
									var req = http.request(processMessageCallOptions, function(response){					
										c_logger.debug("Inside callback");					
										response.on('data', function (chunk) {
											p_commonobject = chunk;
											c_logger.debug("p_commonobject: " + p_commonobject);
										});
										response.on('end', function () {			
											//processMessageCallback(JSON.parse(p_commonobject));
											let response_commonobject = JSON.parse(p_commonobject);							
											c_logger.info("Entering callback function: processUserInteraction method");								
											p_messageobject.send(response_commonobject.responsetext);
											c_logger.info("Exiting callback function: processUserInteraction method");
										});							
									});						
									req.end(JSON.stringify(l_commonobject));
								}catch(p_err){
									c_logger.error("Error in SkypeChannelImpl while making rest call to BotOrchestrator",p_err);
								}
							});
								
							});
						});
					}catch(p_err){
						c_logger.error("Error in SkypeChannelImpl :: getConfigParamsByListUsingNames",p_err);
					}
					
				});
			};	
		
			//Config Params
			let l_paramnames = ['skype-app-id','skype-app-password','skype-webserver-port','sessions-collection','session-timeout-in-minutes'];
			
			try{
				//API call to retreive all the config param values in Map
				c_configurationmanager.getConfigParamsByListUsingNames(p_instancename, l_channelname,l_paramnames,invokeBotService);
			}catch(p_err){
				c_logger.error("Error in SkypeChannelImpl :: getConfigParamsByListUsingNames",p_err);
			}
			
			
		}catch(e)
		{
			c_logger.error("Error occured :: Class - SkypeChannelImpl :: Method - initialiseChannel() ",e);
		}
		
		
	}	
	
}	

module.exports = SkypeChannelImpl;