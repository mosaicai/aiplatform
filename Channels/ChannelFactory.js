'use strict';

var c_instancename='';
var FetchComponent = require('../PlatformAPI/FetchComponent/FetchComponent.js').FetchComponent;
var c_fetchcomponent = new FetchComponent();
var c_logger=require('../PlatformAPI/Utilities/Logger.js')(module);

process.argv.forEach(function (val, index, array) {	
		
	if(array.length == 2)
	{
		c_logger.info("Please provide bot name to initialize!!!!");
		process.exit(1);
		return;			
	}
	
	// Take the bot name as a parameter
	if(index === 2)
	{				
		c_instancename = val;
		c_logger.info("Initializing Bot : " + index);		
	}
});

// Get List of Channel names for the respective Instance Name
// Write API to get list of channel names for given Instance Name and EntityType= "channel"

c_fetchcomponent.getEntityNameByType(c_instancename,'channel',function(p_entitynames)
{
	c_logger.info('Inside callback of getEntityNameByType()');
	try
	{
					if(null != p_entitynames || p_entitynames.length!=0)
					{
						for(var i=0;i<p_entitynames.length;i++)
						{
							let l_channelname = p_entitynames[i];
																
							if(null === l_channelname || 'undefined' === l_channelname || '' === l_channelname || 'none' === l_channelname){
								c_logger.error('There is no Channel configured for this instance');
							}
							else{
								
								var l_requireclasspath = './'+l_channelname+'/'+l_channelname+'Impl.js';
								c_logger.debug('Importing classpath :: '+l_requireclasspath);
								var l_classref = require(l_requireclasspath);
								//c_logger.debug('Importing classref :: '+l_classref);
								var l_classobject = new l_classref(c_instancename);
								c_logger.debug('Importing classobject :: '+l_classobject);
								l_classobject.initialiseChannel(c_instancename, l_channelname,  function(response){
										c_logger.info("Entering callback function : initialiseChannel method");
										c_logger.info("Response from HTML channel : " + response);
										c_logger.info("Exiting callback function : initialiseChannel method");
								});
							}			
						}
					}
					else{
						c_logger.error('No entities have been configured for this instance');
					}
	
	 }
	catch(e)
	{
						c_logger.error("Error occured :: Class - Channel Factory :: Method - getEntityNameByType() ",e);
	} 

});