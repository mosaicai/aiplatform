'use strict';

var FetchComponent = require('../PlatformAPI/FetchComponent/FetchComponent.js').FetchComponent;
var c_fetchcomponent = new FetchComponent();


var c_logger=require('../PlatformAPI/Utilities/Logger.js')(module);

class ProcessorFactory{
	
	
	constructor(){}
	
	// Get an instance of processor
	getProcessorInstance(p_instancename, processorCallback){
		try{
			c_fetchcomponent.getEntityNameByType(p_instancename,'processor',function(p_processorname){
				if(null === p_processorname[0] || 'undefined' === p_processorname[0] || '' === p_processorname[0] || 'none' === p_processorname[0]){
					c_logger.error('Class - ProcessorFactory :: method - getEntityNameByType :: There is no Processor deifined for this instance');
					return null;
				}
				else{
					var l_requireclasspath = __dirname+'/'+p_processorname[0]+'/'+p_processorname[0]+'.js';
					c_logger.debug("Inside processor factory class path :: "+l_requireclasspath);
					var l_classref = require(l_requireclasspath);
					//c_logger.debug("Inside processor factory classref :: "+l_classref);
					var l_classobject = new l_classref(p_instancename);
					c_logger.debug("Inside processor factory class object :: "+l_classobject);
					processorCallback(l_classobject);
				}
			});
		}
		catch(e)
		{
				c_logger.error("Error occurred :: Class - ProcessorFactory :: Method - getProcessorInstance() ",e);
		}
	}
	}

module.exports.ProcessorFactory = ProcessorFactory;