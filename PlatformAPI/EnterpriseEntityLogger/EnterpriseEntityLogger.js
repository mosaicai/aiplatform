'use strict';

var c_logger = require('../Utilities/Logger.js')(module);

var msqlm  = require('../Utilities/MySQLManager.js');
//var c_mysqlmanager = new MySQLManager();

var EntityInstance = require('../EntityStatusManager/EntityInstance.js').EntityInstance;
var c_entityinstance=new EntityInstance();

class EnterpriseEntityLogger{

	constructor(){

	}
	
	insertEntityLog(p_instancename,p_entityname,p_message,p_level,callback){
		if(p_instancename!=null && p_instancename!='' && p_instancename!=undefined && p_entityname!=null && p_entityname!='' && p_entityname!=undefined && p_message!=null && p_message!='' && p_message!=undefined && p_level!=null && p_level!='' && p_level!=undefined){
			c_logger.info("Entering insertEntityLog() entity name :: "+p_entityname+ " for instance name "+p_instancename);

			try{
			
				c_entityinstance.fetchInstanceId(p_instancename,function(p_instanceid){
					c_entityinstance.fetchEntityId(p_entityname,function(p_entityid){
					
							var l_query="SELECT e.`id`,e.`message_destination` FROM entity_message_type e where e.`entity_id`=? and FIND_IN_SET(?,e.`message_type`)";
						var l_bindvariables=[p_entityid,p_level];
					
						msqlm.executeQuery(l_query, l_bindvariables, function(p_error,p_results,p_fields){
					
						if(p_results.length>0)
						{
						c_logger.info("RESULT"+JSON.stringify(p_results[0].message_destination));
						var l_entity_message_type_id=p_results[0].id;
							
							if(p_results[0].message_destination === 'PRIORITY' || p_results[0].message_destination === 'BOTH'){
								var l_insertquery="INSERT INTO entity_messages_priority (entity_message_type_id, instance_id, message, message_date, user_id) values (?,?,?,NOW(),'user_01')";
								var l_insertvalues=[l_entity_message_type_id,p_instanceid,p_message];
								
								msqlm.executeDML(l_insertquery, l_insertvalues, function(p_error,p_results,p_fields){
								
									c_logger.info("RESULT"+JSON.stringify(p_results));	
									callback("SUCCESS");
								});
							}

							if(p_results[0].message_destination === 'LOGS' || p_results[0].message_destination === 'BOTH'){
							
								var l_selectquery="SELECT e.`id` FROM entity_instance e where e.instance_id=? and e.entity_id=? ";
								var l_selectvalues=[p_instanceid,p_entityid];	
								
								msqlm.executeQuery(l_selectquery, l_selectvalues, function(p_error,p_results,p_fields){
									
									if(p_results.length>0){
										c_logger.info("l_entity_instance_id"+JSON.stringify(p_results[0].id));	
										var l_entity_instance_id=p_results[0].id;
										var l_insertquery="INSERT INTO entity_messages_logs (entity_instance_id, log_level, message, message_date, user_id) values (?,?,?,NOW(),'user_01')";
										var l_insertvalues=[l_entity_instance_id,p_level,p_message];
										
										msqlm.executeDML(l_insertquery, l_insertvalues, function(p_error,p_results,p_fields){
											c_logger.info("RESULT"+JSON.stringify(p_results));	
											callback("SUCCESS");
										});
										
									}else{
										c_logger.error("No match found");
									}
								});
							}

						}else{
							c_logger.error("No match found");
						}
							
					});
							
					});	
				});
			
				
			}
			catch(p_err){
					c_logger.error("Error in parameters :: p_instancename("+p_instancename+"),p_entityname("+p_entityname+"),p_message("+p_message+"),p_level("+p_level+")",p_err);
					throw p_err;
			}
			
		}else{
			c_logger.error("Error in parameters :: p_instancename("+p_instancename+"),p_entityname("+p_entityname+"),p_message("+p_message+"),p_level("+p_level+")");
			throw new Error("Error in parameters :: p_instancename("+p_instancename+"),p_entityname("+p_entityname+"),p_message("+p_message+"),p_level("+p_level+")");
		
		}
	}
}

module.exports.EnterpriseEntityLogger=EnterpriseEntityLogger;