'use strict';

// Imports

var c_mongodb = require('mongodb');
var c_assert = require('assert');
var c_messagesCollection='';
var c_logger = require('./Logger.js')(module);
var c_url = '';

// Instantiations
var MongoClient = c_mongodb.MongoClient;

class MongoDBManager{
	
	constructor(p_mongourl)
	{			
		c_url = p_mongourl;
	}
	
	// Insert a message in collection
	insertMessageInCollection(p_message, p_messagesCollection,p_callbackFunction)
	{
	
		if(p_message!=null && p_message!='' && p_message!=undefined){
			c_logger.info("Message to push ###############" + p_message + " to collection "+ p_messagesCollection);
			MongoClient.connect(c_url, function(p_err, db) {
				c_assert.equal(null, p_err);
				db.collection(p_messagesCollection).insert( p_message, function(p_err, p_result) {
					//c_assert.equal(p_err, null);
					c_logger.info('Inserted a message into collection ' + p_messagesCollection);
					p_callbackFunction(true);
					db.close();
				});
			});
		}else{
			c_logger.error("Error in MongoDBManager :: insertMessageInCollection :: p_message("+p_message+") p_messagesCollection("+p_messagesCollection+")");
			throw new Error("Throwing error from insertMessageInCollection");
		}
	
	}
	
	//check if the session already exists
	fetchBotSessionID(p_userId, p_configurationCollection, p_callbackFunction)
	{
		if(p_userId!=null && p_userId!='' && p_userId!=undefined && p_configurationCollection!=null && p_configurationCollection!='' && p_configurationCollection!=undefined){
			c_logger.info('Checking the session for id  ::  ' +p_userId+ "c_url "+c_url);
			MongoClient.connect(c_url, function(p_err, db) {
				c_assert.equal(null, p_err);
				//Fetch the bot configuration
				db.collection(p_configurationCollection).find({'_id': p_userId}).toArray(function (p_err, p_result) {
				
					if (p_err) 
					{
						c_logger.error(p_err);
					}
					else if (p_result.length) 
					{	
						c_logger.debug("The sessionId for user "+p_userId+" is "+p_result);
						p_callbackFunction(p_result);
					}
					else 
					{
						c_logger.info('No document(s) found with defined "find" criteria!');
						p_callbackFunction(false);
						
					}
					
					//Close connection
					db.close();
				});
			});
		}else{
			c_logger.error("Error in MongoDBManager :: fetchBotSessionID :: p_userId("+p_userId+") p_configurationCollection("+p_configurationCollection+")");
			throw new Error("Error in MongoDBManager :: fetchBotSessionID :: p_userId("+p_userId+") p_configurationCollection("+p_configurationCollection+")");
		}
	}
	
	fetchBotUserID(p_userId, p_configurationCollection, p_callbackFunction)
	{
		if(p_userId!=null && p_userId!='' && p_userId!=undefined && p_configurationCollection!=null && p_configurationCollection!='' && p_configurationCollection!=undefined){
			c_logger.debug('Checking if the user id exists ::  ' +p_userId+"configurationCollection"+p_configurationCollection);
			MongoClient.connect(c_url, function(p_err, db) {
				c_logger.info("Inside mongo callback");
				c_assert.equal(null, p_err);
				//Fetch the bot configuration
				db.collection(p_configurationCollection).find({'_id': p_userId}).toArray(function (p_err, p_result) {
					if (p_err) 
					{
						c_logger.error(p_err);
					}
					else if (p_result.length) 
					{	
						c_logger.info('Document found with defined "find" criteria!');
						p_callbackFunction(true);
					}
					else 
					{
						c_logger.info('No document(s) found with defined "find" criteria!');
						p_callbackFunction(false);
						
					}
					
					//Close connection
					db.close();
				});
			});		
		}else{
			c_logger.error("Error in MongoDBManager :: fetchBotSessionID :: p_userId("+p_userId+") p_configurationCollection("+p_configurationCollection+")");
			throw new Error("Error in MongoDBManager :: fetchBotSessionID :: p_userId("+p_userId+") p_configurationCollection("+p_configurationCollection+")");
		
		}
	}
	
	
	
	// find the configuration for a Bot
	fetchBotConfiguration(p_botName, p_configurationCollection, p_callbackFunction)
	{
		if(p_botName!=null && p_botName!='' && p_botName!=undefined && p_configurationCollection!=null && p_configurationCollection!='' && p_configurationCollection!=undefined){
			var l_configuration='';
			c_logger.debug('Fetching the configuration for Bot : ' + p_botName);
			MongoClient.connect(c_url, function(p_err, db) {
				c_assert.equal(null, p_err);
				//Fetch the bot configuration
				db.collection(p_configurationCollection).find({'botconfiguration.botname': p_botName}).toArray(function (p_err, p_result) {
					if (p_err) 
					{
						c_logger.error(p_err);
					}
					else if (p_result.length) 
					{				
						// If p_result is not null and we have only one record for the configuration
						if(null != p_result && 1 == p_result.length)
						{
							l_configuration = p_result[0];
							c_logger.debug(l_configuration);
							// Invoke the passed callback function with the configuration as a parameter
							//p_callbackFunction(configuration);
						}
					}
					else 
					{
						c_logger.info('No document(s) found with defined "find" criteria!');
						
					}
					p_callbackFunction(l_configuration);
					//Close connection
					db.close();
				});
			});		
		}else{
			c_logger.error("Error in MongoDBManager :: fetchBotConfiguration :: p_botName("+p_botName+") p_configurationCollection("+p_configurationCollection+")");
			throw new Error("Error in MongoDBManager :: fetchBotConfiguration :: p_botName("+p_botName+") p_configurationCollection("+p_configurationCollection+")");
		}
	}
	
	// find the configuration for a Bot
	deleteBotConfiguration(p_botName, p_configurationCollection, p_callbackFunction)
	{	
		if(p_botName!=null && p_botName!='' && p_botName!=undefined && p_configurationCollection!=null && p_configurationCollection!='' && p_configurationCollection!=undefined){
			c_logger.info('Deleting the configuration for Bot : ' + p_botName);
			MongoClient.connect(c_url, function(p_err, db) {
				c_assert.equal(null, p_err);
				//Fetch the bot configuration
				db.collection(p_configurationCollection).deleteMany({'botconfiguration.botname': p_botName},function (p_err, p_result) {
					var l_metaDataDeleted=false;
					var l_configuration='';
					
					if(!p_err)
					{
						l_metaDataDeleted = true;
					}
					c_logger.debug("MetaDataDeleted for bot name :"+p_botName + " : "+l_metaDataDeleted);
					p_callbackFunction(l_metaDataDeleted);
					db.close();
				});
			});
		}else{
			c_logger.error("Error in MongoDBManager :: deleteBotConfiguration :: p_botName("+p_botName+") p_configurationCollection("+p_configurationCollection+")");
			throw new Error("Error in MongoDBManager :: deleteBotConfiguration :: p_botName("+p_botName+") p_configurationCollection("+p_configurationCollection+")");
		
		}
	}	
	
	
	// find the configuration for a Bot
	fetchLastCommunication(p_botName, p_configurationCollection, p_callbackFunction)
	{
		if(p_botName!=null && p_botName!='' && p_botName!=undefined && p_configurationCollection!=null && p_configurationCollection!='' && p_configurationCollection!=undefined){
			var l_configuration='';
			c_logger.debug('Fetching the configuration for Bot : ' + p_botName);
			MongoClient.connect(c_url, function(p_err, db) {
				c_assert.equal(null, p_err);
				//Fetch the bot configuration
				db.collection(p_configurationCollection).find({'_id': p_botName}).toArray(function (p_err, p_result) {
					if (p_err) 
					{
						c_logger.error(p_err);
					}
					else if (p_result.length) 
					{				
						// If p_result is not null and we have only one record for the configuration
						if(null != p_result && 1 == p_result.length)
						{
							l_configuration = p_result[0];
							c_logger.debug(l_configuration.lastTransactionDatetime);
							// Invoke the passed callback function with the configuration as a parameter
							//p_callbackFunction(configuration);
						}
					}
					else 
					{
						c_logger.info('No document(s) found with defined "find" criteria!');
					}
					p_callbackFunction(l_configuration.lastTransactionDatetime);
					//Close connection
					db.close();
				});
			});		
			
		}else{
			c_logger.error("Error in MongoDBManager :: fetchLastCommunication :: p_botName("+p_botName+") p_configurationCollection("+p_configurationCollection+")");
			throw new Error("Error in MongoDBManager :: fetchLastCommunication :: p_botName("+p_botName+") p_configurationCollection("+p_configurationCollection+")");
		
		}	
	}
	
	updateInCollection(p_id, p_collection,p_timestamp,p_callbackFunction)
	{
	
		if(p_id!=null && p_id!='' && p_id!=undefined && p_collection!=null && p_collection!='' && p_collection!=undefined && p_timestamp!=null && p_timestamp!='' && p_timestamp!=undefined ){
			c_logger.debug("Updating the last transaction time field with :: "+new Date(p_timestamp)+" :: where the session ID is :: "+p_id);
			MongoClient.connect(c_url, function(p_err, db) {
				c_assert.equal(null, p_err);
				db.collection(p_collection).update({_id: p_id}, {$set: {lastTransactionDatetime: new Date(p_timestamp)}}, function (p_err, p_numUpdated) {
				if (p_err) {
						c_logger.error(p_err);
				} else if (p_numUpdated) {
						c_logger.debug('Updated Successfully %d document(s).', p_numUpdated);
				} else {
						c_logger.debug('No document found with defined "find" criteria!');
				}
					//Close connection
					db.close();
				});
			});
		}else{
			c_logger.error("Error in MongoDBManager :: updateInCollection :: p_id("+p_id+") p_collection("+p_collection+") p_timestamp("+p_timestamp+")");
			throw new Error("Error in MongoDBManager :: updateInCollection :: p_id("+p_id+") p_collection("+p_collection+") p_timestamp("+p_timestamp+")");
		}
	}
	
	
	updateSessionIdCollection(p_id, p_collection,p_sessionId)
	{
		if(p_id!=null && p_id!='' && p_id!=undefined && p_collection!=null && p_collection!='' && p_collection!=undefined){
			c_logger.debug("Updating the sessionId field where user Id is  :: "+p_id+" :: with :: "+p_sessionId);
			MongoClient.connect(c_url, function(p_err, db) {
				c_assert.equal(null, p_err);
				db.collection(p_collection).update({_id: p_id}, {$set: {sessionID: p_sessionId}}, function (p_err, p_numUpdated) {
				if (p_err) {
						c_logger.error(p_err);
				} else if (p_numUpdated) {
						c_logger.debug('Updated Successfully %d document(s).', p_numUpdated);
				} else {
						c_logger.info('No document found with defined "find" criteria!');
				}
					//Close connection
					db.close();
				});
			});
		}else{
			c_logger.error("Error in MongoDBManager :: updateSessionIdCollection :: p_id("+p_id+") p_collection("+p_collection+") p_sessionId("+p_sessionId+")");
			throw new Error("Error in MongoDBManager :: updateSessionIdCollection :: p_id("+p_id+") p_collection("+p_collection+") p_sessionId("+p_sessionId+")");
		
		}
	}
	
}
module.exports.MongoDBManager = MongoDBManager;
