//'use strict';

//function to trim path
module.exports.trimPath = function(str, chr, skipLevels, callback){
		for(i=skipLevels;i>0;i--)
		{
			str = str.substring(0,str.lastIndexOf(chr));
		}
		callback(str);
	};
	
module.exports.getTime = function(hrtime)
	{
		//hrtime is an array containing high precision time. first element [0] contains seconds and [1] contains nanoseconds 
		return ( (hrtime[0] * 1000000) + (hrtime[1] / 1000) ) / 1000; //convert seconds to microseconds & nano second to microseconds, add both numbers and convert to milli seconds
	}
	