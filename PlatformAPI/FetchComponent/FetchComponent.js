'use strict';

/* var MySQLManager = require('../Utilities/MySQLManager.js').MySQLManager;
var c_mysqlManager = new MySQLManager(); */

var msqlm = require('../Utilities/MySQLManager.js');

var c_logger = require('../../PlatformAPI/Utilities/Logger.js')(module);
var c_conn;
//var c_entity_names = [];

class FetchComponent
{
			constructor()
		{
				
		}
		//fetching the entity name by using the instance names and entity types
		 getEntityNameByType(p_instanceName,p_entityType,callback)
		{	
		
			if(p_instanceName!=null && p_instanceName!='' && p_instanceName!=undefined && p_entityType!=null && p_entityType!='' && p_entityType!=undefined){
			let l_entity_names = [];
				c_logger.info('Entering getEntityNameByType()');
					try
					{
					
						var l_query="SELECT * FROM entity e join entity_instance ei join instance i where e.id=ei.entity_id AND i.id=ei.instance_id AND i.instance_name=? AND e.entity_type=?";
						var l_bindvariables=[p_instanceName,p_entityType];
						msqlm.executeQuery(l_query, l_bindvariables, function(p_error,p_results,p_fields){	


							 if (p_error){
								c_logger.error(p_error);
								try
								{
									c_logger.debug("Inside try of getEntityNameByType ");
								}
								catch (p_error) {
									c_logger.error("Error while closing connection inside getEntityNameByType() "+p_error.stack);
								}
								throw p_error; 
								 
							}else{ 
									if(p_results.length>0)
									{
										c_logger.debug("Retrieving records for instance "+p_instanceName+" where the entity type is "+p_entityType);
										for(var i=0;i<p_results.length;i++)
										{
											c_logger.debug("Entity Name : "+p_results[i].entity_name);
											l_entity_names.push(p_results[i].entity_name);
										}	
										callback(l_entity_names);	
									}
									else{
										c_logger.error("Entity name does not exist for this instance");
										//throw p_error;
										//throw new Error("Entity name does not exist for this instance");
									}
								}
						});
					}
					catch(p_error)
					{
						c_logger.error("Error occured while query execution inside getEntityNameByType :: parameters: p_instanceName("+p_instanceName+"), p_entityType("+p_entityType+")",p_error);
						throw p_error;
						
					}
					
				c_logger.info('Exiting getEntityNameByType()');
										
			}else{
				c_logger.error("Error for the parameters: p_instanceName("+p_instanceName+"), p_entityType("+p_entityType+")");
				c_logger.info('Exiting getEntityNameByType()');
				throw new Error("Error for the parameters: p_instanceName("+p_instanceName+"), p_entityType("+p_entityType+")");
			
			
			}
		}


}
module.exports.FetchComponent = FetchComponent;
