'use strict';
var msqlm = require('../Utilities/MySQLManager.js');

var c_logger = require('../../PlatformAPI/Utilities/Logger.js')(module);
var p_conn;

class EntityInstance
{


constructor(){
	
	}

//checking if the state is relevant for the entity
checkEntityStateRelevance(p_instanceid ,p_entityid, p_state, callback) 
{


	if(p_instanceid!=null && p_instanceid!='' && p_instanceid!=undefined && p_entityid!=null && p_entityid!='' && p_entityid!=undefined && p_state!=null && p_state!='' && p_state!=undefined){
		c_logger.info("Entering checkEntityStateRelevance() entity Id :: "+p_entityid+ " for state "+p_state);
		try{
			var l_stateid ='';
			
			var l_query="select es.id FROM entity_states es join entity e where e.id=es.entity_id AND e.id=? AND es.state=?";
			var l_bindvariables=[p_entityid,p_state];
			msqlm.executeQuery(l_query, l_bindvariables, function(p_error,p_results,p_fields){	
			
								c_logger.debug("Printing size of result set :: "+p_results.length);							
								//c_logger.debug(" Printing the state id "+rows[0].id);
								
								if(p_results.length == 0)
								{
									c_logger.debug("This state is not relevant for the entity");
								}
								
								else if(p_results.length !=0 && p_results[0].id != null && p_results[0].id != '')
								{
										 l_stateid = p_results[0].id;
																		
								}
								
								callback(l_stateid);
			
			
			});
			
			
		}
		catch(p_error)
		{
			c_logger.error("Error occured while executing query inside checkEntityStateRelevance() parameters: p_instanceId("+p_instanceId+"), p_entityId("+p_entityId+"), p_state("+p_state+")",p_error);
			throw p_error;
		}
		c_logger.info("Exiting checkEntityStateRelevance()");
		
	}else{
		c_logger.error("Error for the parameters: p_instanceId("+p_instanceId+"), p_entityId("+p_entityId+"), p_state("+p_state+")");
		c_logger.info("Exiting checkEntityStateRelevance()");
		throw new Error("Error for the parameters: p_instanceId("+p_instanceId+"), p_entityId("+p_entityId+"), p_state("+p_state+")");
	
	}
}


	
//get the entity_instance_id from the entity id and the instance id
retrieveEntityInstanceId(p_instanceid,p_entityid,callback)
{

	if(p_instanceid!=null && p_instanceid!='' && p_entityid!=null && p_entityid!=''){
			c_logger.info("Entering retrieveEntityInstanceId()");	
			try{
				var l_query="SELECT ei.id from entity_instance ei where ei.instance_id=? AND ei.entity_id=?";
				var l_bindvariables=[p_instanceid,p_entityid];
				msqlm.executeQuery(l_query, l_bindvariables, function(p_error,p_results,p_fields){	
					if(p_results[0].id != null && p_results[0].id != '')
					{
						var l_entityinstanceid = p_results[0].id;
						c_logger.debug("Printing the entity instance id : "+p_results[0].id);
						callback(l_entityinstanceid);
					}
					else{
						c_logger.info("Could not find the entity instance id");
					}	
				});
			}
			catch(p_error)
			{
				c_logger.error("Error while executing query inside retrieveEntityInstanceId() parameters: p_instanceId("+p_instanceId+"), p_entityId("+p_entityId+")",p_error);
				throw p_error;
			}
			c_logger.info("Exiting retrieveEntityInstanceId()");		
	}else{
	
		c_logger.error("Error for the parameters: p_instanceId("+p_instanceId+"), p_entityId("+p_entityId+")");
		c_logger.info("Exiting retrieveEntityInstanceId()");
		throw new Error("Error for the parameters: p_instanceId("+p_instanceId+"), p_entityId("+p_entityId+")");
	
	}
}


//update the state across the entity_instance_id
updateState(p_entityinstanceid,p_stateid,callback)
{	

	if(p_entityinstanceid!=null && p_entityinstanceid!='' && p_stateid!=null && p_stateid!=''){
			c_logger.info("Entering updateState()");	
				try{
				c_logger.debug("Updating the state id"+p_stateid+" for entity instance id "+p_entityinstanceid);		

				var l_updatequery="update instance_states set state_id = ?, last_updated_on = NOW() where entity_instance_id=?";
				var l_bindvariablesforupdate=[p_stateid,p_entityinstanceid];
				msqlm.executeDML(l_updatequery, l_bindvariablesforupdate, function(p_error,p_results,p_fields){	
					c_logger.debug("Affected Rows from Updation : "+p_results[0].affectedRows);
									
					//if the record to update doesnt exist , insert it 
					if(p_results[0].affectedRows == 0)
					{
						var l_insertquery="insert into instance_states(last_updated_on,entity_instance_id,state_id,user_id) values(NOW(),?,?,'user_01')";
						var l_bindvariablesforinsert=[p_entityinstanceid,p_stateid];
						msqlm.executeDML(l_insertquery, l_bindvariablesforinsert, function(p_error,p_insertedRows,p_fields){	
							
								c_logger.debug("Inserting the record  : "+JSON.stringify(p_insertedRows[0]));
								callback("SUCCESS");
						});
				
					}
					else{
							c_logger.info("updation occured");
							callback("SUCCESS");
					}
				
				});
				
			}catch(p_error)
			{
				c_logger.error("Error occured while executing query inside updateState() :: parameters: p_entityinstanceid("+p_entityinstanceid+"), p_stateid("+p_stateid+") ",p_error);
				throw p_error;
			}
			c_logger.info("Exiting updateState()");		

	}else{
		c_logger.error("Error for the parameters: p_entityinstanceid("+p_entityinstanceid+"), p_stateid("+p_stateid+")");
		c_logger.info("Exiting updateState()");
		throw new Error("Error for the parameters: p_entityinstanceid("+p_entityinstanceid+"), p_stateid("+p_stateid+")");
	}
}


fetchInstanceId(p_instancename,callback)
{
	if(p_instancename!=null && p_instancename!=''){
		c_logger.info("fetchInstanceId()  begins" +p_instancename);
		
		try{
			var l_query="select i.id from instance i where i.instance_name=?";
			var l_bindvariables=[p_instancename];
			
			msqlm.executeQuery(l_query, l_bindvariables, function(p_error,p_results,p_fields){		
					
					if(p_results.length>0){
						c_logger.info("Instance Id "+p_results[0].id);
						var l_instanceid = p_results[0].id;
						callback(l_instanceid);
					}else{
						c_logger.info("error occured  inside fetchInstanceId()");
					
					}
					
			});
		}catch(p_error){
				
			c_logger.error("error occured while closing connection inside fetchInstanceId() :: parameters: p_instancename("+p_instancename+")",p_error);
			throw p_error;
		}
		c_logger.info("fetchEntityId ends");
		
	}else{
		c_logger.error("Error for the parameters: p_instancename("+p_instancename+")");
		c_logger.info("Exiting fetchInstanceId()");
		throw new Error("Error for the parameters: p_instancename("+p_instancename+")");
	
	
	}	
}

fetchEntityId(p_entityname,callback)
{
	
	if(p_entityname!=null && p_entityname!=''){
	
		c_logger.info("fetchEntityId()  begins" +p_entityname);
		
			try{
				var l_query="select e.id from entity e where e.entity_name=?";
				var l_bindvariables=[p_entityname];
				
				msqlm.executeQuery(l_query, l_bindvariables, function(p_error,p_results,p_fields){	
					
					if(p_results.length>0){
						c_logger.info("Entity Id "+p_results[0].id);
						var l_entityid = p_results[0].id;
						callback(l_entityid);
					}else{
						c_logger.info("error occured  inside fetchEntityId()");
					
					}
				
				});
			
			}catch(p_error){
				c_logger.error("error occured while closing connection inside fetchEntityId() ::parameters: p_entityname("+p_entityname+") ",p_error);
				throw p_error;
				
			}
			
			c_logger.info("fetchEntityId ends");
	}else{
		c_logger.error("Error for the parameters: p_entityname("+p_entityname+")");
		c_logger.info("Exiting fetchEntityId()");
		throw new Error("Error for the parameters: p_entityname("+p_entityname+")");
	
	}
}

}
module.exports.EntityInstance = EntityInstance;