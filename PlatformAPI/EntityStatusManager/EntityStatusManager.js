'use strict';
var c_logger = require('../../PlatformAPI/Utilities/Logger.js')(module);

var msqlm = require('../Utilities/MySQLManager.js').MySQLManager;

var EntityInstance = require('./EntityInstance.js').EntityInstance;
var c_entityinstance = new EntityInstance();
var c_conn;

class EntityStatusManager
{
		constructor(){
			
		}
			
						
		//method to update the state of a particular entity if relevant for the same
		 updateEntityState(p_instancename,p_entityname,p_state,callback)
		{	
				c_logger.info("Inside updateEntityState");
				
				var l_entityid;
				var l_instanceid;
				
				c_logger.info("Changing state of "+p_instancename+" - "+p_entityname);
				
				//getting the mysql connection object
				//c_mysqlmanager.getConnection(function(connection){
				//c_conn = connection;
				//c_logger.debug("Connection retrieved ::  "+c_conn);
												
				c_logger.info("Entering updateEntityState()");
					
				//fetching the instance Id from the instance name
				c_entityinstance.fetchInstanceId(p_instancename,function(p_instanceid){
					
					l_instanceid = p_instanceid;
						
					//fetching the entity id from the entity name
					c_entityinstance.fetchEntityId(p_entityname,function(p_entityid){
					
							l_entityid = p_entityid;
							
									//checking with the entity id if the state is relevant for it
								try{	
									c_entityinstance.checkEntityStateRelevance(l_instanceid,l_entityid,p_state.key,function(p_stateid){
																			
									if(p_stateid != "" && p_stateid != undefined ){
										try{	
											//if the state is relevant for the entity then retrieve it entity instance id
											c_entityinstance.retrieveEntityInstanceId(l_instanceid,p_entityid,function(p_entityinstanceid){
											
													var l_entityinstanceid = p_entityinstanceid;
													var l_stateid = p_stateid;
												
													//update the state id accross the retrieved entity instance id
													c_entityinstance.updateState(l_entityinstanceid,l_stateid ,function(p_statusmessage){
															c_logger.info(p_statusmessage);									
															callback(p_statusmessage);
													});
													
												});
											
											}catch(e){
												c_logger.error(" Error in EntityStatusManager :: retrieveEntityInstanceId ",e);
												throw e;
											}
										}
										else{
											c_logger.info("State not relevant for the entity");
										}	
				
									});
								}catch(e){
									c_logger.error("Error in EntityStatusManager :: checkEntityStateRelevance",e);
									throw e;
								}
							
							});
						
					});
							
				c_logger.info("Exiting updateEntityState()");
			//});
			
		
		}
	}
module.exports.EntityStatusManager = EntityStatusManager;
