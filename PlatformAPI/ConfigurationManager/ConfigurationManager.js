'use strict';

var msqlm = require('../Utilities/MySQLManager.js');

var c_logger = require('../../PlatformAPI/Utilities/Logger.js')(module);


class ConfigurationManager
{
				
	constructor(){
	
	}
	
	//method to fetch the param value by giving parameter names
	 getConfigParamByName(p_instanceName,p_entityName,p_paramName,callback)
	{	
	
		if(p_instanceName!=null && p_instanceName!='' && p_instanceName!=undefined && p_entityName!=null && p_entityName!='' && p_entityName!=undefined && p_paramName!=null && p_paramName!='' && p_paramName!=undefined){
			c_logger.info('Entering getConfigParamByName()');
			try{
				
				var l_query="SELECT a.entity_attribute_name,i.value from entity e join entity_attributes a join entity_instance_parameters i join instance s join entity_instance t where i.entity_attribute_id = a.id AND a.entity_id = e.id AND i.entity_instance_id = t.id AND t.instance_id = s.id AND s.instance_name = ? AND e.entity_name=? AND a.entity_attribute_name=?";
				var l_bindvariables=[p_instanceName,p_entityName,p_paramName];
				msqlm.executeQuery(l_query, l_bindvariables, function(p_error,p_results,p_fields){	
				
					if(p_results.length>0)
						{
							for(var i=0;i<p_results.length;i++)
								{
								c_logger.debug("The value for "+p_results[i].entity_attribute_name +" is "+p_results[i].value);
								}
							callback(p_results[0].value);
							
						}
						else{
							c_logger.info("No records retrieved for this criteria: p_instanceName("+p_instanceName+"), p_entityName("+p_entityName+"), p_paramName("+p_paramName+")");
						}
				
				
				}); 
				
			}
			catch (p_error) {
				c_logger.error("Error during query execution inside getConfigParamByName() :: parameters: p_instanceName("+p_instanceName+"), p_entityName("+p_entityName+"), p_paramName("+p_paramName+")",p_error);
				throw p_error;
			}
			c_logger.info('Exiting getConfigParamByName()');
		
		
		}else{
		
			c_logger.error("Error for the parameters: p_instanceName("+p_instanceName+"), p_entityName("+p_entityName+"), p_paramName("+p_paramName+")");
			c_logger.info('Exiting getConfigParamByName()');
			throw new Error("Error for the parameters: p_instanceName("+p_instanceName+"), p_entityName("+p_entityName+"), p_paramName("+p_paramName+")");
		}
		
	}
	
	
	//method to fetch parameter values by giving parameter Ids
	 getConfigParamById(p_instanceId,p_entityId,p_paramName,callback)
	{	
	
		if(p_instanceId!=null && p_instanceId!='' && p_entityId!=null && p_entityId!='' && p_paramName!=null && p_paramName!=''){
			c_logger.info('Entering getConfigParamById()');
			
			try{
				
				var l_query="SELECT a.entity_attribute_name,i.value from entity e join entity_attributes a join entity_instance_parameters i join instance s join entity_instance t where i.entity_attribute_id = a.id AND a.entity_id = e.id AND i.entity_instance_id = t.id AND t.instance_id = s.id AND s.id = ? AND e.id=? AND a.entity_attribute_name=?";
				var l_bindvariables=[p_instanceId,p_entityId,p_paramName];
				msqlm.executeQuery(l_query, l_bindvariables, function(p_error,p_results,p_fields){	
					if(p_results.length>0)
					{
						for(var i=0;i<p_results.length;i++)
						{
						c_logger.debug("The value for "+p_results[i].entity_attribute_name +" is "+p_results[i].value);
						}
						callback(p_results[0].value);	
					}
					else{
						c_logger.info("No records retrieved for this criteria: p_instanceId("+p_instanceId+"), p_entityId("+p_entityId+"), p_paramName("+p_paramName+")");
					}
			
				});
			}
			catch (p_error) {
				c_logger.error("Error while executing query inside getConfigParamById() :: parameters: p_instanceId("+p_instanceId+"), p_entityId("+p_entityId+"), p_paramName("+p_paramName+")",p_error);
				throw p_error;
			}
			c_logger.info('Exiting getConfigParamById()');
		
		}else{
			c_logger.error("Error for the parameters: p_instanceId("+p_instanceId+"), p_entityId("+p_entityId+"), p_paramName("+p_paramName+")");
			c_logger.info('Exiting getConfigParamById()');
			throw new Error("Error for the parameters: p_instanceId("+p_instanceId+"), p_entityId("+p_entityId+"), p_paramName("+p_paramName+")");
		}
	}
	
	//fetching list of values by passing the instance name , entity name, array of param names
	 getConfigParamsByListUsingNames(p_instanceName,p_entityName,p_paramNames,callback)
	{
	
		
	
			if(p_instanceName!=null && p_instanceName!='' && p_entityName!=null && p_entityName!='' && p_paramNames!=null && p_paramNames!=''){
			
				c_logger.info('Entering  getConfigParamsByListUsingNames()');
				var l_conn;
				var l_params_map = new Map();  
				var l_param_names='';
					try{
							for(var i=0;i<p_paramNames.length;i++)
							{
								if(i!=(p_paramNames.length-1))
								{
									l_param_names = "'"+p_paramNames[i]+"',"+l_param_names ;
								}
								else
								{
									l_param_names = l_param_names+"'"+p_paramNames[i]+"'";
								}
								
							}
							c_logger.debug("Getting values for the following parameters :: "+l_param_names);
						
							var l_query="SELECT a.entity_attribute_name,e.value FROM entity_instance_parameters e join entity_instance t join entity_attributes a join instance s join entity et where t.id= e.entity_instance_id AND a.id=e.entity_attribute_id AND s.id=t.instance_id AND t.entity_id=et.id AND s.instance_name = ? AND et.entity_name=? AND a.entity_attribute_name IN ("+l_param_names+")";
							var l_bindvariables=[p_instanceName,p_entityName];
							msqlm.executeQuery(l_query, l_bindvariables, function(p_error,p_results,p_fields){
						
								if(p_results.length>0)
								{
									for(var i=0;i<p_results.length;i++)
									{
										l_params_map.set(p_results[i].entity_attribute_name,p_results[i].value);
									}
									
									l_params_map.forEach(function (item, key, mapObj) {  
											c_logger.debug("Fetched values are :: "+key.toString()+"-->"+item.toString());  
									});  

										callback(l_params_map);
								}
								else{
									c_logger.info("No records retrieved for this criteria: p_instanceName("+p_instanceName+"), p_entityName("+p_entityName+"), p_paramNames("+p_paramNames.toString()+")");
								}
							
							
							});					
											
						}
					catch (p_error)
					{
						c_logger.error("Error while executing query inside getConfigParamsByList() parameters: p_instanceName("+p_instanceName+"), p_entityName("+p_entityName+"), p_paramNames("+p_paramNames.toString()+")",p_error);
						throw p_error;
					}
					c_logger.info('Exiting getConfigParamsByList()');
			
			}else{
				
				c_logger.error("Error for the parameters: p_instanceName("+p_instanceName+"), p_entityName("+p_entityName+"), p_paramNames("+p_paramNames.toString()+")");
				c_logger.info('Exiting getConfigParamsByList()');
				throw new Error("Error for the parameters: p_instanceName("+p_instanceName+"), p_entityName("+p_entityName+"), p_paramNames("+p_paramNames.toString()+")");
				
			}
			
	}
	
	//fetching list of values by passing the instance id , entity id, array of param names
	 getConfigParamsByListUsingIds(p_instanceId,p_entityId,p_paramIds,callback)
	{
	
		if(p_instanceId!=null && p_instanceId!='' && p_entityId!=null && p_entityId!='' && p_paramIds!=null && p_paramIds!=''){
			c_logger.info('Entering getConfigParamsByListUsingIds()');
			var l_conn;
			var l_params_map = new Map();  
			var l_paramIds='';
				try{
						
						for(var i=0;i<p_paramIds.length;i++)
						{
							if(i!=(p_paramIds.length-1))
							{
								l_paramIds = "'"+p_paramIds[i]+"',"+l_paramIds ;
							}
							else
							{
								l_paramIds = l_paramIds+"'"+p_paramIds[i]+"'";
							}
							
						}
						c_logger.debug("Getting values for the following parameters :: "+l_paramIds);
						
						var l_query="SELECT a.entity_attribute_name,e.value FROM entity_instance_parameters e join entity_instance t join entity_attributes a join instance s join entity et where t.id= e.entity_instance_id AND a.id=e.entity_attribute_id AND s.id=t.instance_id AND t.entity_id=et.id AND s.id = ? AND et.id= ? AND a.id IN (?)";
						var l_bindvariables=[p_instanceId,p_entityId,l_paramIds];
						msqlm.executeQuery(l_query, l_bindvariables, function(p_error,p_results,p_fields){
							if(p_results.length>0)
							{
								for(var i=0;i<p_results.length;i++)
								{
									l_params_map.set(p_results[i].entity_attribute_name,p_results[i].value);
								}
								
								l_params_map.forEach(function (item, key, mapObj) {  
									c_logger.debug("Fetched values are :: "+key.toString()+"-->"+item.toString());  
								});  

									callback(l_params_map);
							}
							else{
								c_logger.info("No records retrieved for this criteria: p_instanceId("+p_instanceId+"), p_entityId("+p_entityId+"), p_paramIds("+p_paramIds.toString()+")");
							}
						
						});
						
					
				
						
					}
				catch (p_error)
				{
					c_logger.error("Error while executing query inside getConfigParamsByListUsingIds()  parameters: p_instanceId("+p_instanceId+"), p_entityId("+p_entityId+"), p_paramIds("+p_paramIds.toString()+")",p_error);
					throw p_error;
				}
				c_logger.info('Exiting getConfigParamsByList()');

			
		}else{
		
			c_logger.error("Error for the parameters: p_instanceId("+p_instanceId+"), p_entityId("+p_entityId+"), p_paramIds("+p_paramIds.toString()+")");
			c_logger.info('Exiting getConfigParamsByList()');
			throw new Error("Error for the parameters: p_instanceId("+p_instanceId+"), p_entityId("+p_entityId+"), p_paramIds("+p_paramIds.toString()+")");
		
		}
			
			
	}
}
module.exports.ConfigurationManager = ConfigurationManager




