'use strict';

// Load properties
var c_logger = require('../../PlatformAPI/Utilities/Logger.js')(module);

//Load Entity Status Manager
var EntityStatusManager = require('../../PlatformAPI/EntityStatusManager/EntityStatusManager.js').EntityStatusManager;

//Load Entity states enum
var ComponentStatus = require('../../PlatformAPI/Utilities/ComponentStatus.js').ComponentStatus;

//Loading EnterpriseEntityLogger
var EnterpriseEntityLogger = require('../../PlatformAPI/EnterpriseEntityLogger/EnterpriseEntityLogger.js').EnterpriseEntityLogger;
var c_enterpriseentitylogger = new EnterpriseEntityLogger();

let l_entityname = 'AlexaAi';

// A utility class to interact with API ai methods
class AlexaAiImpl{

	constructor(p_instancename)
	{
		c_enterpriseentitylogger.insertEntityLog(p_instancename,l_entityname,'STARTED AIENGINE','ENTITY_STATE',function(success){
			c_logger.info("Entity log Inserted for (instance:"+p_instancename+", entity:"+l_entityname+"):"+success);
		}); 
	}
	//var obj={"salesperson":"John Smith"}
	
	
	handleAIInteraction(p_messageobject, callbackFunction)
	{
		if(p_messageobject!=null && p_messageobject!=undefined && p_messageobject!=''){
	
			var aiResponse = new Object();
			aiResponse.language='en';
			var key=[];
			var value=[];
			var obj={};
			c_logger.debug("p_messageobject"+JSON.stringify(p_messageobject));
			if(p_messageobject.intentrequest.intentName != null){
				aiResponse.action=p_messageobject.intentrequest.intentName;
			}
			if(p_messageobject.intentrequest.slots!= null){
				var slots=p_messageobject.intentrequest.slots;
				var arrKey=Object.keys(slots);
				for(var key in slots) {
					key = slots[key].name;
					value = slots[key].value;
					
					obj[key]=value;
					c_logger.debug("keys "+JSON.stringify(key));
					c_logger.debug("values "+JSON.stringify(value));
					
				}
				c_logger.debug("aiResponse Object "+JSON.stringify(obj));
				aiResponse.parameters=obj;
				c_logger.debug("aiResponse.parameters "+aiResponse.parameters.salesPerson);
					
			}
			if(p_messageobject.intentrequest.intentName != null){
				aiResponse.intentName=p_messageobject.intentrequest.intentName;
			}
			
			callbackFunction(aiResponse);
		}else{
			c_logger.error("Error in AlexaAiImpl: handleAIInteraction :: p_messageobject ");
			throw new Error("Error in AlexaAiImpl: handleAIInteraction :: p_messageobject ");
		}
		
	}
}


module.exports = AlexaAiImpl;