'use strict';
// Packages import
var apiai = require('apiai');

// Load properties
var c_logger = require('../../PlatformAPI/Utilities/Logger.js')(module);

//Load Entity Status Manager
var EntityStatusManager = require('../../PlatformAPI/EntityStatusManager/EntityStatusManager.js').EntityStatusManager;
var c_entitystatusmanager = new EntityStatusManager();

//Load Entity states enum
var ComponentStatus = require('../../PlatformAPI/Utilities/ComponentStatus.js').ComponentStatus;

//Loading EnterpriseEntityLogger
var EnterpriseEntityLogger = require('../../PlatformAPI/EnterpriseEntityLogger/EnterpriseEntityLogger.js').EnterpriseEntityLogger;
var c_enterpriseentitylogger = new EnterpriseEntityLogger();

//Module for fetching configurations
var ConfigurationManager = require('../../PlatformAPI/ConfigurationManager/ConfigurationManager.js').ConfigurationManager;
var c_configurationmanager = new ConfigurationManager();

//Get the language token
const LanguageIdentifier = require('../../CoreServices/LanguageIdentifier.js').LanguageIdentifier;
var c_languageidentifier = new LanguageIdentifier();

var app;
var token;
let l_entityname = 'ApiAi';
let c_instancename = '';

// A utility class to interact with API ai methods
class ApiAiImpl{
	
	// Constructor
	constructor(p_instancename)	
	{		
		c_instancename=p_instancename;
		c_enterpriseentitylogger.insertEntityLog(p_instancename,l_entityname,'STARTING AIENGINE','ENTITY_STATE',function(success){
			c_logger.info("Entity log Inserted for (instance:"+p_instancename+", entity:"+l_entityname+"):"+success);
		}); 
		c_logger.info('Starting aiengine.');		
		c_entitystatusmanager.updateEntityState(p_instancename,l_entityname,ComponentStatus.STARTING,function(){
			// Get config for l_aiadaptertoken			
			
			
				c_configurationmanager.getConfigParamByName(p_instancename, l_entityname, 'apiaitoken', function(p_apiaitoken){
					try{
						app = apiai(p_apiaitoken);
						c_logger.info('Started aiengine.');
						c_entitystatusmanager.updateEntityState(p_instancename,l_entityname ,ComponentStatus.STARTED,function(){
							token = p_apiaitoken;
							c_enterpriseentitylogger.insertEntityLog(p_instancename,l_entityname,'STARTED AIENGINE','ENTITY_STATE',function(success){
								c_logger.info("Entity log Inserted for (instance:"+p_instancename+", entity:"+l_entityname+"):"+success);
							});
						});				
					}
					catch(err){
						c_logger.error('Error:'+err.message);
					}	
				});			
			
			
		}); 		
	}
	
	// Handle basic API interaction
	//handleAIInteraction(p_messageobject, language, callbackFunction)
	handleAIInteraction(p_messageobject, callbackFunction)
	{

	
		c_languageidentifier.findTongue(p_messageobject.text,p_messageobject.instancename, function(p_language){
				c_logger.info("Entering handleAIInteraction method");
				c_logger.info('Language passed to '+l_entityname+' is ' + p_language);

				c_enterpriseentitylogger.insertEntityLog(p_messageobject.instancename,l_entityname,'Language passed to '+l_entityname+' is ' + p_language,'DEBUG',function(success){
					c_logger.info("Entity log Inserted for (instance:"+p_messageobject.instancename+", entity:"+l_entityname+"):"+success);
				});
				
				let random_session_id = p_messageobject.sessionid;
				var reqOptions={lang:p_language, sessionId:random_session_id};
				var request = app.textRequest(p_messageobject.text , reqOptions);
				var aiResponse = new Object();
				aiResponse.language=p_language;
				var response = "";
				
				request.on('response', function(response) {
					
					c_logger.info('Response from '+l_entityname+': '+response);
					c_enterpriseentitylogger.insertEntityLog(p_messageobject.instancename,l_entityname,'Response from '+l_entityname+': ' + response,'DEBUG',function(success){
						c_logger.info("Entity log Inserted for (instance:"+p_messageobject.instancename+", entity:"+l_entityname+"):"+success);
					});
					
					aiResponse.success = true;
					
					if(null != response && null != response.result){
						
						// Whether the action is incompleted	
						if(null != response.result.actionIncomplete){
							aiResponse.actionIncomplete = response.result.actionIncomplete;
						}	

						if(null != response.result.resolvedQuery) {
							 aiResponse.resolvedQuery = response.result.resolvedQuery;
						}
						
						// What are the parameters
						if(null != response.result.parameters){
							aiResponse.parameters = response.result.parameters;
						}	
						
						// what is the action
						if(null != response.result.action){
							aiResponse.action = response.result.action;
						}	
						
						// What is the response text
						if(null != response.result.fulfillment && null != response.result.fulfillment.speech){
							aiResponse.responseText = response.result.fulfillment.speech;
						}			
						
						// What is the intent
						if(null != response.result.metadata.intentName){
							aiResponse.intentName = response.result.metadata.intentName;
						}
						
						// What is the context
						if(null != response.result.contexts){
							aiResponse.contexts = response.result.contexts;
						}
						
						//what is session ids
						if(null != response.sessionId) {
							aiResponse.sessionId = response.sessionId;  //currentSession.resolvedQuery = '';
						}
						
						//what is the asked query
						if(null != response.resolvedQuery) {
							aiResponse.resolvedQuery = response.resolvedQuery;  //currentSession.resolvedQuery = '';
						}
						
						if('' === aiResponse.responseText){
							response = "";
							c_logger.info('Blank response from APi Ai ---');
							c_logger.info('End of APi Ai call----');
							c_logger.info("Exiting handleAIInteraction method");
							callbackFunction(aiResponse);
						}
						else{	
							response = aiResponse.responseText;
							c_logger.info("Exiting handleAIInteraction method");
							callbackFunction(aiResponse);
							c_logger.info('End of APi Ai call----');
						}
							
					}
				});
				
				request.on('error', function(error) {
					c_logger.error(error);	
					aiResponse.responseText = 'Hi, there was an issue connecting with the chat server, please try again in some time';	
					c_logger.info("Exiting handleAIInteraction method");
					callbackFunction(aiResponse, random_session_id);             			 
				});
				
				request.end();		
				
		});
	}		
}
module.exports = ApiAiImpl;










