'use strict';

// Load properties
var c_logger = require('../../PlatformAPI/Utilities/Logger.js')(module);

//Load Entity Status Manager
var EntityStatusManager = require('../../PlatformAPI/EntityStatusManager/EntityStatusManager.js').EntityStatusManager;

//Load Entity states enum
var ComponentStatus = require('../../PlatformAPI/Utilities/ComponentStatus.js').ComponentStatus;

//Loading EnterpriseEntityLogger
var EnterpriseEntityLogger = require('../../PlatformAPI/EnterpriseEntityLogger/EnterpriseEntityLogger.js').EnterpriseEntityLogger;
var c_enterpriseentitylogger = new EnterpriseEntityLogger();

let l_entityname = 'GoogleHomeAi';
// A utility class to interact with API ai methods
class GoogleHomeAiImpl{

	constructor(p_instancename)
	{
		c_enterpriseentitylogger.insertEntityLog(p_instancename,l_entityname,'STARTED AIENGINE','ENTITY_STATE',function(success){
			c_logger.info("Entity log Inserted for (instance:"+p_instancename+", entity:"+l_entityname+"):"+success);
		});
	}
	
	handleAIInteraction(p_messageobject, callbackFunction)
	{
		if(p_messageobject!=null && p_messageobject!=undefined && p_messageobject!=''){
				var aiResponse = new Object();
				aiResponse.language='en';
				// Whether the action is incompleted	
				if(null != p_messageobject.intentrequest.actionIncomplete){
					aiResponse.actionIncomplete =p_messageobject.intentrequest.actionIncomplete;
				}	

				if(null != p_messageobject.intentrequest.resolvedQuery) {
					 aiResponse.resolvedQuery = p_messageobject.intentrequest.resolvedQuery;
				}
				
				// What are the parameters
				if(null != p_messageobject.intentrequest.parameters){
					aiResponse.parameters = p_messageobject.intentrequest.parameters;
				}	
				
				// what is the action
				if(null != p_messageobject.intentrequest.action){
					aiResponse.action = p_messageobject.intentrequest.action;
				}	
				
				// What is the response text
			    if(null != p_messageobject.intentrequest.fulfillment && null != p_messageobject.intentrequest.fulfillment.speech){
					aiResponse.responseText = p_messageobject.intentrequest.fulfillment.speech;
				}			
				
				// What is the intent
				if(null != p_messageobject.intentrequest.metadata.intentName){
					aiResponse.intentName = p_messageobject.intentrequest.metadata.intentName;
				}
				
				// What is the context
				if(null != p_messageobject.intentrequest.contexts){
					aiResponse.contexts = p_messageobject.intentrequest.contexts;
				}
				
				//what is session ids
				if(null != p_messageobject.intentrequest.sessionId) {
					aiResponse.sessionId = p_messageobject.intentrequest.sessionId;  //currentSession.resolvedQuery = '';
				}
		
			callbackFunction(aiResponse);
		
		}else{
			c_logger.error("Error in GoogleHomeAiImpl: handleAIInteraction :: p_messageobject ");
			throw new Error("Error in GoogleHomeAiImpl: handleAIInteraction :: p_messageobject ");
		}
	}
}


module.exports= GoogleHomeAiImpl;