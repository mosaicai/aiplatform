'use strict'



// Initializations
//var ApiAiImpl=require('./ApiAi/ApiAiImpl.js').ApiAiImpl;

var c_logger=require('../PlatformAPI/Utilities/Logger.js')(module);

//To be changed
var p_configparammap_test = [];
p_configparammap_test['p_aiadaptertoken']='68c27e562583416abac6ababc7103e6d';

//Module for fetching component name
var FetchComponent = require('../PlatformAPI/FetchComponent/FetchComponent.js').FetchComponent;
var c_fetchcomponent = new FetchComponent();

class AIFactory
{	
	constructor()
	{	
	}
	
	// Get the correct AI handler based on passed parameter
	getAiHandler(p_instancename,aifactoryCallback){
		c_logger.info("Entering getAiHandler callback");				
	try{
		c_fetchcomponent.getEntityNameByType(p_instancename,'aiengine',function(p_entitynames){

		
				if(null === p_entitynames || 'undefined' === p_entitynames || '' === p_entitynames || 'none' === p_entitynames){
						c_logger.error('There is no AI handler defined');
					}
					else{
						for(var i=0;i<p_entitynames.length;i++){
					
						let l_entityname = p_entitynames[i];
						
						if(null === l_entityname || 'undefined' === l_entityname || '' === l_entityname || 'none' === l_entityname){
							c_logger.error('There is no Channel configured for this instance');
							return null;
						}
						

						else{								
							let l_requireclasspath = './'+l_entityname+'/'+l_entityname+'Impl.js';
							let l_classref = require(l_requireclasspath);				
							let l_classobject = new l_classref(p_instancename);								
							c_logger.info("Exiting getAiHandler callback");								
							aifactoryCallback(l_classobject);															

						}
					
					}
				}
		});	

	}//end of try
	catch(e)
	{
		c_logger.error("Error occured :: Class - AIFactory :: Method - getAiHandler() ",e);
	}
		c_logger.info("Exiting AIFactory");	
	}	
}


module.exports.AIFactory = AIFactory;